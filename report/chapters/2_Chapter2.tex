\chapter{State-of-the-Art}

\section{Context}

Many domains are intensively using remote execution engines~: Workflow managers,
tasks and jobs schedulers, DevOps, and reproducible research.
Figure~\ref{fig:fields} shows a graphical representation of how those fields are
related and table~\ref{table:sum_func} summary the extracted functionalities we
want to be able to provide.

\subsection{Remote execution tools}

The core activity of a remote execution tool is to make distant nodes execute
programs. Those programs can be configuration tools in order to prepare a
platform for a specific execution, simulation/programs to be run or clean-up
programs to clean the computation units after executions.
The most simple way to create a remote execution tool is to use a script looping
over an ssh command, making any computer in the list execute a specific command.
As this processing can be heavily parallelised, one can use a sliding window in
order to work on $x$ connections at the same time. A great majority of tools are
doing this, but it as been proved inefficient and a distributed mechanism is
more profitable. A distributed mechanism is one that uses particular nodes on
the network in order to increase the parallel window. Usually those nodes are
also in the bucket of those used for the remote execution. In the remote
execution field, those specific nodes hold a running daemon running the same
code as the root one and there are different strategies to start the daemon on
the remote nodes. The daemon can be either always running on some key nodes and
contacted when a remote execution is needed or can be started on demand. For
those that are started on demand, two strategies are applied. Or the remote
execution tools send its code on the distant node as an extra step or the code
is already installed on the remote computer. The self-deploying mechanism
provides strong independence regarding the remote computer. Lesser configuration
is needed on the remote, and, better the tool is portable and provide flexibility.

Many improvements have been made in the literature and two
main tools have been built. Clustershell~\cite{thiell2012clustershell} and
Taktuk~\cite{claudel2009taktuk} are made to offer good properties such
as scalability, reliability, efficient remote command execution, results
gathering and other. Thus we will not present tools like
pdsh~\cite{claudel2009taktuk} that are less efficient.

\subsection{Tasks/Jobs Managers}

Centralised or distributed resources and tasks managers are tools used to
manage large platforms. They come with scheduling capabilities and their purpose
is to exploit computation nodes, making scheduling, execution, and monitoring.
Those tools eventually use execution engines to contact and make computers
execute jobs.

\subsection{Workflow Managers}

A workflow is the automation of a process~\cite{liu2015survey}. There are two
kinds of workflows~: business and scientific ones. Business workflows are
task-oriented and Scientific workflows are data-oriented.
%
Business workflows are a set of activities linked together in order to optimise
and orchestrate execution. Scientific workflows are used to model and run
experiments on computational resources. The most general representation of a
workflow is the Direct Acyclic Graph (DAG) which is a graphical representation
of the tasks to execute. DAGs are translated into tasks and later scheduled by
an external scheduler.
%
Workflow managers are large pieces of software made of several layers~:
presentation, user services, workflow execution plan generation, and
execution~\cite{liu2015survey}. The three firsts layers are used to prepare the
execution plan that will be and compiled optimised before the execution layer.
The execution layer takes care in the general case of the scheduling, tasks
execution and the fault tolerance.
%
Fault tolerance can be classified in two kinds, proactive and
reactive~\cite{liu2015survey}. Reactive failure tolerance is about relaunching
failed simulations. Proactive failure tolerance is about redundancy to handle
possible failures. 
%
As computation becomes more and more complex on supercomputers, workflows have
to deal with more and more steps and need underlying layers able to deal with
this increasing amount of work.
%
As examples of workflows we can find Pegasus\footnote{https://pegasus.isi.edu/},
Taverna\footnote{http://www.taverna.org.uk/},
Kepler\footnote{https://kepler-project.org/},
DIET\footnote{http://graal.ens-lyon.fr/diet/?page\_id=551} and
others~\cite{deelman2015pegasus}.

\subsection{DevOps}

DevOps is a practice involving fast and flexible development and platform
administration \cite{zhu2016devops}. A DevOps has to efficiently integrate
development, delivery, and operations with fluidity between those separate
competence fields~\cite{ebert2016devops}\cite{zhu2016devops}. They are using a
lot of tools in order to achieve those tasks, from automatic tests bench, to
automatic deployment and reconfiguration to achieve very short development
cycles. In this field, cookbooks are used to pilot orchestration tools like
Ansible\footnote{https://www.ansible.com/},
Puppet\footnote{https://puppet.com/fr}, Chef\footnote{https://www.chef.io/chef/}
among others~\cite{liu2016building}. Those tools are remote execution programs
by themselves with flavours or policies dedicated to them. An interesting idea
from the DevOps field is the philosophy deifying infrastructure as data. The
physical layer is abstracted. And we can use infrastructure the same way that we
use software~\cite{johann2017kief}. Important points of the DevOps field are
traceability and auditability.~\cite{johann2017kief}

\subsection{Reproducible Search and Tools}

In reproducible research, the key idea is to have a tool able to understand a
procedure and relaunch it every time it is needed, tools like
Expoi\footnote{http://expo.gforge.inria.fr/},
Plush/Glush\footnote{https://www.planet-lab.org/},
Execo\footnote{http://execo.gforge.inria.fr/doc/latest-stable/},
XPFlow\footnote{http://xpflow.gforge.inria.fr/} among
others~\cite{buchert2015survey}. All of those tools are execution engines plus
some policies that make them attached to specific kinds of platforms.
Unfortunately, for some authors, those tools suffer from coupling problems.
They are too attached to some grid philosophies to be used in a more general
way. And some researchers tend to use their own scripts for remote
execution\cite{stanisic2015effective}.

\begin{figure}[!t]
    \includegraphics[width=1.0\linewidth]{pics/fields.png}
    \caption{Dependences between domains using remote execution tools.}\label{fig:fields}
\end{figure}

\subsection{Common Requirements}

From workflow managers, DevOps, tasks/jobs mangers, reproducible research, and
InSitu applications, we can extract a list of specific needed functionalities.
We will briefly present those here and summarise them in the
table~\ref{table:sum_func}.

\subsubsection{High Level Interpretation}

High level interpretation mean to offer the final user "user friendly" interfaces
to setup its execution plan. A high level interpretation language can either be
a graphical view of an execution as a Direct Acyclic Graph (DAG) or a
description using a Domain Specific Language (DSL).
%
The \textbf{DAG Interpretation} consists of extracting a list of steps to
execute, to validate their coherency, and to give them to an underlying layer
able to schedule the steps as tasks/jobs.
%
Having user need validation. \textbf{Input validation} means for
instance to compile user entry to ensure termination of its program or its
correctness.

\subsubsection{Remote Execution and Platform Control}

Controlling a platform is knowing every characteristic of a range of computers,
to have control of them, such as, reboot, making configurations, to be able to
exploit them with some job scheduling. Or to expose API for a tool to discover
the platform in order to ask resources reservations.
%
\textbf{Job and Task scheduling} is having a list of jobs/tasks to process,
to choose the right place at the right time to execute them. Complex
algorithms are used to optimise user waiting time and platform utilisation.
%
Jobs schedulers use specific \textbf{platforms support} to be able to make node
reservation or job monitoring. Platform managers can expose \textbf{Resources
discovery} APIs to help other programs to make reservations and to schedule
tasks.

\subsubsection{Execution Management and Fault Tolerance}

At a point, executions need to be managed. \textbf{Results Analysis} allows a
user to visualise using tools its execution results. \textbf{Interactive
Execution} helps the user to debug his applications. \textbf{Provenance
tracking} allows one to gather information about every configuration and
hardware details on his execution. It is used by computer scientists to
understand important impacting parameters. For programs to be executed, specific
environments sometimes must be setup. Often used by tasks/jobs managers in order
to prepare nodes before execution, \textbf{verification and configuration} is
checking the state of a computational resource and configuring it.

Platforms and executed codes may suffer from failures such as instabilities or
bugs. Running large code on HPC centers may take several days of computation and
cannot be restarted from scratch at every problem. Solutions are made to provide
fault tolerance. \textbf{Checkpointing} is a solution that periodically saves
the state of the simulation on disk. Upon crashing, the simulation will be
restarted from the last valid checkpoint. \textbf{Redundancy} is another way to
be fault tolerant. Redundance supposes that an execution is made several times,
then, if one fall other can still be used.

\subsubsection{Code Coupling}

Due to the increasing complexity of computations and to reduce disk usage,
people are focusing nowadays on InSitu computing. InSitu means "in place", which
in the simulation context means to keep the data close to its production source
and not to use a slow intermediary like disk storage for instance. Those
techniques consist in coupling existing codes in order to transit information
when they are still in memory. Bredala~\cite{dreher2016bredala} is a InSitu
middleware enabling simulations coupling with N to M patterns. Bredala is using
MPI as its communication layer to multiplex simulations. MPI is not dynamic, and
simulations cannot be added on the fly to an existing coupling. This problem is
highlighted by Bredala's author. Solutions like Melissa bring new ways with
client/server patterns to multiplex simulations.

\begin{table}
    \center
    \begin{tabular}{| p{3cm} | p{2.3cm} | l | p{2cm} | p{2cm} | p{1.7cm} |}
        \hline
        \midrule
        functionality                  & Reproducible research & DevOps & Workflow managers & Tasks/Jobs managers & InSitu \\ \hline
        DAG Interpretation             &                       &        &         X         &                     &        \\ \hline
        Job/Task Scheduling            &                       &        &         X         &         X           &        \\ \hline
        Remote execution               &         X             &   X    &         X         &         X           &        \\ \hline
        Results Analysis               &         X             &   X    &         X         &                     &        \\ \hline
        Input Validation               &         X             &   X    &         X         &                     &        \\ \hline
        Platforms support              &                       &        &                   &         X           &        \\ \hline
        Ressources discovery           &                       &   X    &                   &         X           &        \\ \hline
        Provenance tracking            &         X             &        &                   &                     &        \\ \hline
        Checkpointing                  &         X             &        &         X         &                     &        \\ \hline
        Verification and configuration &                       &   X    &                   &         X           &        \\ \hline
        Interactive Execution          &         X             &   X    &         X         &                     &        \\ \hline
        Code Coupling                  &                       &        &                   &                     &    X   \\ \hline
        \hline
    \end{tabular}
    \caption{Functionalities repartition over the different fields}
    \label{table:sum_func}
\end{table}

\section{Related Work}

We will focus this related work using the required functionalities highlighted
in the previous section.

\subsection{High Level Interpretation}

High level interpretation helps users to translate their wishes to script a
remote execution engine. For instance, workflow managers translate DAGs to
execution engines. The paper~\cite{deelman2009workflows} gives a good overview
of the execution model for workflow managers. Pegasus maps workflows on
different targets like, PBS, LSF, Condor, and individual machines. Pegasus uses
the DAGMan workflow engine which interfaces to Condor scheduling queues.
%
From the DevOps world, Ansible, Puppet and
Chef~\cite{liu2016building}\cite{ebert2016devops} are configuration management
tools, all working with recipes. Those recipes are sometimes written in DSL
languages, or sometimes directly in a programming language (ruby for Chef). Due
to their file-based configuration files, they are static tools made for
administration purposes. Thus, if the group notion is embedded in those DSL it
is only made to serve general administration purposes. For instance, a bunch of
computers will be dedicated to run data-base software, another bunch will be
dedicated to run web servers. Computers are not intended to move from a group to
another quite often.
%
For reproducible research, domain specific languages (DSL) are often used as an
interface for the user. This is the case of EXPO, OMF, or XPflow per instance.
Plush/Glush, requires a XML configuration file, which is not far from a DSL.
Those tools are made to expose a research oriented API. They provide facilities
to interact with tasks and resources managers, making them platform dependent.
%
For the rest of the interpretation, the remote execution script, is mainly made
of command line like Taktuk and clustershell. As for Ansible, Chef and
Puppet Clustershell which embed a notion of group but need to be statically
declared in configuration files. TakTuk is more in the Unix philosophy, a Swiss
knife understanding complex command lines or interpreting its standard input to
receive commands.

Command line tools present the advantage to be more simple to interface with.
Tools asking for static configuration files to run may impact the dynamism of
the tool we want to build. We would prefer a tool interpreting commands
at run-time on its standard input. Expo shows that it is possible to interpret
DSL and to translate it over command line tools.

\subsection{Remote Execution and Platform Control}

OAR~\cite{capit2005batch}, developed at LIG, uses
Taktuk~\cite{claudel2009taktuk} to remotely contact nodes and provide a strong
independency between OAR and the computation nodes. Taktuk is used to deploy
images on nodes and to make deep configuration of the software stack on each
computation node.
%
Slurm~\cite{yoo2003slurm} another task manager uses its own daemon-based
architecture to control computational node. Slurm is more administration
dependent then OAR as it needs to be installed on each computational node.
%
Flux~\cite{ahn2014flux} asks the question of another way to manage HPC centers
with a scalable distributed approach. Flux offers job scheduling and execution
and a way for launched programs to access easily a distributed data base, that
may be used as a way for them to communicate. Flux uses a tree-based topology to
communicate between daemons using Zmq\footnote{http://zeromq.org/}.
%
In order to be scalable, Flux's distributed approach seems natural. For Slurm --
and for clustershell --, having a installed daemon on each node increase the
administration cost of the platform. For Clustershell, not every node needs to be
installed in advance, only group leaders. We still tend to prefer the OAR
control on nodes using TakTuk as it is mostly an administration less tool.
Ansible also only requires an ssh connexion to configure nodes.
%
Like Flux, from a scalability point of view Puppet is an interesting tool,
because it seems to be able to use a tree-based communication layer.


%Plush/Glush embeds ressources discovery and acquisition, application deployment
%and maintenance.
%\begin{itemize}
%    \item Abstract notion of job. A job is an instance that can run a single
%        application or management services allowing recursion. This model forms
%        the foundation for hierarchical, multilevel resource management and job
%        scheduling.
%    \item Job Hierarchy Model: Tree based hierachy of Flux jobs.
%    \item Generalized Resource Model: FLux does not focus only on agregation of
%        nodes as a basic ressources but uses a more general encapsulation
%        allowing it to manage other kind of ressources like data storage per
%        instance.
%    \item Multilevel Resource Elasticity Model: Each Flux job can grow and
%        shrink according to their needs and the needs of their children.
%    \item Common Scalable Communication Infrastructure Model: All Flux instances
%        communicate with their parents and children and transit information by
%        message brokers.
%\end{itemize}
%
%Flux embeds a key value distributed data store. This KVS is used as a
%communication module. This module is exposed to launched MPI programs through a
%library.

\subsection{Execution Management and Fault Tolerance}

In order to manage execution, remote execution tools need to gather information
about executions. It is done depending on the needs of the upper layers.
%
DAGMan used by Pegasus does not interact with jobs independently but reads the
logs of the remote execution engine to keep track the status of the jobs.
%
Ansible, Puppet, and Chef suffer from a lack of control of the execution cycle.
And they do not collect execution results~\cite{buchert2015survey}. They only
offer information about the success or the failure of a step.
%
For reproducible research, Expo, XPFlow, and Execo three related tools that use
Taktuk as a base layer to contact nodes and execute commands, thus making those
able to have a fine grain control over executions. Fine grain control enables
message sending, standard input control, and life-cycle control of a remote
process.
%
More closely, TakTuk and Clustershell, are two highly parallel remote execution
engines able to execute commands on remote computers and gather execution
results. Taktuk gather every command results and node status to the its root.
This feedback gathering is important to take decisions on a remote execution.
%
%Expo A well integrated tool on Grid'5000~\cite{imbert2013using}. Based on
%Taktuk. A general tool for scripting. Handles files and local and remote
%processes. Centralised tool made to scale.
%
Current workflow systems' fault tolerance, exception handling and recovery are
ad hoc tasks undertaken by individual workflow designers rather than being part
of the systems themselves. Triana passively warn the user when an error occurs
and lets him debug the workflow. Using a smart re-run feature it can avoid
unnecessary computation. Like Pegasus, Askalon supports fallback, task-level
recovery, checkpointing, and workflow-level redundancy. Plush/Glush has the
capacity to manage failures, through a constant stream of information from
every node involved. Taktuk detects nodes failure and keeps a constant feedback
about executed programs.
%
Flux uses Zmq~\cite{hintjens2011omq} as a communication layer between its nodes.
Zmq is a high level communication library that provides some glue around TCP to
make it more reliable. It also provides high level communication patterns that
ease the development of distributed applications.

To apply different fault tolerance strategies, one needs at least to detect
faults. Having a constant feedback at a basic layer allows decision taking for
fault tolerance. It brings a failure detector by analysing timeouts on
communications between nodes and by analysing remote execution return code.

\subsection{Code coupling}

Code coupling is a strategy to make InSitu programming. It avoid useless
writings and disk use to transfer data directly from a program to another.
Bredala~\cite{dreher2016bredala} is an in situ middleware offering split and
merge capabilities for data structures. It relies on MPI to connect different
programs and this makes its weakness regarding flexibility.
%
FlowVr is a virtual reality middleware with interesting design. Each node
embeds a daemon to which sub-processes will register in order to communicate with
others through an event-based programming model. FlowVr relies on a separate
launcher to deploy its daemons and is not dynamic, no applications can join the
runtime after lunch.
%
Melissa is an InSitu library running as a server for parametric studies. Melissa
uses Zmq for the communication layer between client and server application.
Melissa's main weakness is using the file system for the client to discover the
server. Melissa is dynamic, many clients can bootstrap and connect to the server
to send data without knowing when and where they will be executed.
%
To multiplex simulations and/or visualisation tools, different approaches are
used, a common one is to build a meta application using MPI to connect different
MPI codes into one big application, or less common is to use an external
communication library to link programs together. Both solutions bring their
challenges. From the MPI point of vue, it is a complex task to associate
existing codes into a large fully functional one. Codes need to be coupling
ready, for instance, comWorld is forbidden.
%
This solution also suffers from a lack of flexibility at runtime, everything
needs to be prepared in advance and the running configuration needs to be well
setup.
%
As MPI suffers also from a lack of failure tolerance, a single sick node in the
runtime takes down the entire application and it may be very hard to build a
correct checkpoint mechanism for complex workflows.
%
With Exascale platforms the number of nodes involved in simulation will continue
to grow, increasing the probability of failures per time unit, and the need of a
more reliable way to connect MPI codes will quickly emerge.

Using MPI to couple simulations together is not a solution for the next platform
generation. Coupled simulation's part need to be able to fall without restarting
all the application. TakTuk enables remotely executed programs to communicate
with each other on a common communication channel. Each TakTuk's launched
program has an address on a special communication channel. Currently, for a
remote program, there is no way to discover the address of another one on the
communication channel, thus, it does not allow two programs having no knowledge
about the run-time to exchange messages. Adding some glue around may enable one
to develop those capabilities.

\section{Summary}

As a reminder, we want to be scalable in order to fill the Exascale gap,
%
to manage aggregates of computation units as groups dedicated to executions,
%
to bring control plane communication facilities to enable easy implementation of
InSitu programs,
%
to provide a centralised control point to have a constant feedback over the
whole execution plan,
%
and to be highly dynamic in order to add or remove aggregates at run time
allowing dynamic client server applications.

Tools interpreting DSL or DAG are too much dependent on static configurations
and our groups need to be dynamic. Thus we cannot tend to use tools not
providing strong independence regarding configuration. TakTuk and ClusterShell,
two command lines tools are preferred choices because they are able to contact
ranges of computers with some parameters on a command line and to keep
connexions open in order to be exploited on run time.

To be scalable we need to focus on a strategy like Flux, Puppet, ClusterShell
and TakTuk does. Having a communication tree between daemons should be as much
administration-less as possible in order to manage stock operating system. This
platform independence enables a future user to develop specific platform
support. Furthermore, TakTuk shows that having a tool with few dependencies with
auto-propagation features can be efficient.

Regarding fault tolerance, Plush/Glush furnish a constant feedback that allow
one to develop every fault tolerance mechanism over it. We should avoid the
non-existent information collection of DevOps or workflow managers tools. TakTuk
also provides a way to have interactive executions on remote nodes. Our remote
execution tool should do the same.

TakTuk possesses a way for remotely executed programs to discuss on an external
communication layer. Flux seems to offer a way for remotely executed programs
to have access to a distributed database. Both solution may enable to provide
code coupling.
