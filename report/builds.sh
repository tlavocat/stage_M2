#!/bin/bash
xelatex -file-line-error -halt-on-error template-report.tex 2>&1 | grep --color=auto -E "Warning|Missing|$|Error|Undefined";
bibtex template-report
xelatex -file-line-error -halt-on-error template-report.tex 2>&1 | grep --color=auto -E "Warning|Missing|$|Error|Undefined";
