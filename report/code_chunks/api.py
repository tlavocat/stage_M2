class Sample(TaskProcessor):

    def register_tasks(self) :
        # Start a new group, with computers A and B. Group leader
        # attached to the root.
        g1 = Group("g1", "g1", "A,B", "0", "root")
        g2 = NumberedGroup("g2", "g2", "A,A,A,A", "0", "root")

        # Make a simple broadcast on first group
        # b1 termination does not implies g1 termination.
        b1 = Broadcaster("b1", "uptime", g1)
        # the broadcast will start when the group g1 is in the running
        # state
        b1.add_dependency({g1:consts.RUNING}, True)

        # serial broadcast after b1 on g1, will close g1 after
        # termination
        # b2 termination implies g1 termination.
        b2 = SerialBroadcaster("b2", ["uptime", "pwd"], g1, True)
        # the broadcast will start when the broadcast b1 is DONE
        b2.add_dependency({b1:consts.DONE}, True)

        # Make a simple broadcast with a timeout of 30 seconds
        b3 = Broadcaster("b3", "sleep 1000", g2, True, 30)
        # b3 will start when g1 is done and g2 is running
        b3.add_dependency({g1:consts.DONE,
                                       g2:consts.RUNING}, True)

        # register all the generated tasks
        self.tasks.append(g1)
        self.tasks.append(g2)
        self.tasks.append(b1)
        self.tasks.append(b2)
        self.tasks.append(b3)
