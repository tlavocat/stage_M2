class Grid5000Test(FrameworkControler):

    def start(self, networkId):
        # After connexion
        def network_root_up(error_nodes):
            # After numbering
            def network_root_update(data):
                # After Broadcast is done
                def done(data) :
                    print("done {}".format(data))
                    self.erebor.terminate()
                    self.close()
                # Make each node in the group execute the
                # uptime command.
                # Get notified on the termination on done
                self.broadcast_exec_on("0",
                                       "uptime",
                                       consts.TRUE,
                                       "0",
                                       "root",
                                       "0",
                                       "root",
                                       done)
            # Number each node inside the root group
            self.network_update_on(consts.TRUE, "0", "root", "0", "root",
                                   network_root_update)
        # Makes the root group connect node_list nodes.
        # Get notified at the end of operation on network_root_up
        self.spawn_on("0",self.node_list, consts.FALSE, "0", "root",
                      "0", "root", network_root_up)
