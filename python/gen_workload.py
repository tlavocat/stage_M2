#!/usr/bin/env python3
import sys
import socket
from isengard import Helper

def main(argv):
    import getopt
    try:
        options, argv = getopt.getopt(
                argv[1:],
                "p:w:f:i:m:M:s:",
                [
                    "program=", "who=", "folder=", "input=","min=","max=","steps="
                    ]
                )


        xmin, xmax  = 0, 1
        step        = 50
        node_list   = ""
        folder_base = ""
        who         = "taktuk,erebor"
        pgm         = ""

        for option, value in options:
            if option in ["-p", "--program"]:
                pgm = value
            elif option in ["-w", "--who"]:
                who = value
            elif option in ["-m", "--min"]:
                xmin = int(value)
            elif option in ["-M", "--max"]:
                xmax = int(value)
            elif option in ["-s", "--steps"]:
                step = int(value)
            elif option in ["-i", "--input"]:
                nodes = value;
            elif option in ["-f", "--folder"]:
                folder_base = value+"/"

        helper    = Helper()
        node_list = helper.build_list(nodes)
        node_len  = len(node_list)

        script = open("launch.sh", 'w')

        for local_max in range(xmin, xmax+1) :
            if local_max % step == 0 :
                f = open(folder_base+str(local_max), 'w')
                for j in range(0, local_max) :
                    f.write(node_list[j%node_len]+"\n")
                f.close();
                if "erebor" in who :
                    script.write("/usr/bin/time -f \"%e\" -o "+folder_base+"erebor"+str(local_max)
                        +".out -a ./launcher "+pgm+"  -f "
                        +folder_base+""+str(local_max)
                        +" --time-file="+folder_base+""+str(local_max)+".time"
                        +"\n")
                if "taktuk" in who :
                    script.write("/usr/bin/time -f \"%e\" -o "+folder_base+"taktuk"+str(local_max)
                        +".out -a taktuk"
                        +" -o connector=\'\"connector:$host;$peer;$line\\n\"\' -o state=\'\"state:$host;$position;$rank;$count\".event_msg($line).\"\\n\"\'"
                        +" -f "+folder_base+""+str(local_max)
                        +" -e synchronize broadcast exec [ uptime ]\n")
        script.close()

    except getopt.GetoptError as message:
        sys.stdout.write("toto")

if __name__ == "__main__":
    sys.exit(main(sys.argv))
