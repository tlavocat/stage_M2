#!/usr/bin/env python3.5
import sys
import getopt
import time
import re
import signal
import json
from isengard           import consts
from isengard.isengardc import Isengard
from isengard.consts    import bcolors

def main(argv):
    try:
        options, argv = getopt.getopt(
                argv[1:],
                "d:i:n",
                [ "debug=", "id=", "non-root" ]
                )
    except getopt.GetoptError as message:
        sys.stdout.write("toto")

    log  = []
    for option, value in options:
        if option in ["-d", "--debug"]:
            if value == "*" :
                log = ["erebor", "network", "isengard", "wrapper",
                "unix_socket", "bridge"]
            else :
                log = value.split(",")

    isengard = Isengard(True, log)
    isengard.go()


    def signal_handler(a, b):
        isengard.terminate()
        sys.exit(0)

    # TESTS TODO, To remove later
    def tests():
#        isengard.spawn_nodes("0", "A,B,K", True, True)
#        time.sleep(1)
#        isengard.network_renumber(True)
#        time.sleep(1)
#        isengard.request_network_status(True)
#        isengard.spawn_nodes("1", "C,D", True, True)
#        time.sleep(1)
#        isengard.spawn_nodes("1", "E,F", True, True)
#        time.sleep(1)
#        isengard.network_update(True)
#        time.sleep(1)
#        isengard.network_renumber(True)
#        time.sleep(1)
#        isengard.network_update(True)
#        #isengard.wait_reduce("all", True)
#        #isengard.network_renumber(True)
#        #time.sleep(1)
#        isengard.request_network_status(True)
#        isengard.execute_on("0", "uptime", True)
#        isengard.execute_on("1", "uptime", True)
#        isengard.execute_on("2", "uptime", True)
        print("test messages sizes")
        for i in range(0, 10000) :
            message = ''.join(['0' for s in range(0, i+1)])
            message = str(i)+" : "+message
            isengard.send_message_to("0", "all", message, True)

    signal.signal(signal.SIGINT, signal_handler)
    tests()
    isengard.join()

if __name__ == "__main__":
    sys.exit(main(sys.argv))
