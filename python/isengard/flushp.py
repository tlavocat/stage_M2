import sys
def flushprint(*args, **kwargs):
    print(*args, file=sys.stdout, **kwargs)
    sys.stdout.flush()
def flushprint_nothing(*args, **kwargs):
    pass
