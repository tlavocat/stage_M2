import struct
import sys
import json
import select
import queue
import socket
import os
import flushp
from isengard  import consts
from threading import Lock
from threading import Thread

# Class Unix Socket
# Author : Thomas Lavocat
#
# Make the bridge between the main program and taktuk communication device
# through files.
#
# Everything that comes and go on this socket should be encoded in valid JSON.
class Unix_socket(Thread):

    # need a Wrapper instance to communicate with and a function to callback
    # when something arrive on the socket.
    def __init__(self, taktuk, recv_callback, debug_list):
        Thread.__init__(self)
        self.taktuk = taktuk
        self.debug_list = debug_list
        self.debug      = "unix_socket" in self.debug_list
        if self.debug :
            self.flushprint = flushp.flushprint
        else :
            self.flushprint = flushp.flushprint_nothing

        # As many of this program can run on the machine, find the next
        # available control_adress file to work with
        count = 0;
        found = False
        while not found :
            try:
                self.control_adress = "/tmp/control_adress"+str(count);
                # find the next available communication file
                while os.path.exists(self.control_adress):
                    count+=1;
                    self.control_adress = "/tmp/control_adress"+str(count);
                os.mknod(self.control_adress)
                os.unlink(self.control_adress)
                # Init the control socket
                self.server_socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
                self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                self.server_socket.bind(self.control_adress)
                self.server_socket.setblocking(0)
                self.server_socket.listen(5)
                found = True
            except:
                found = False

        self.recv_callback = recv_callback

        self.socket_output = None
        self.ready_send = False
        self.sendLock   = Lock()
        # acquire until connection initialized by the launched process
        self.sendLock.acquire()
        self.daemon = True
        self.poll   = True

    # ask taktuk to launch the bridge program
    def launch_bridge(self):
        debug = ""
        if len(self.debug_list) > 0 :
            debug = "--debug="+(",".join(self.debug_list))
        cmd ='0 exec [ socket_bridge.py --socket '+self.control_adress+" "+debug+" ] "
        self.taktuk.send_command(cmd)

    def run(self):
        epoll          = select.epoll()
        epoll.register(self.server_socket.fileno(), select.EPOLLIN)
        self.out_epoll = select.epoll()
        # launch the bridge
        try:
            connections = {};
            self.flushprint("ready to poll")
            while self.poll :
                s_header_m     = None
                s_rest_header  = 0
                s_buffered_m   = None
                s_rest_to_read = 0
                events         = epoll.poll(consts.timeout)
                for fileno, event in events:
                    #self.flushprint("event to handle")
                    if fileno == self.server_socket.fileno():
                        self.flushprint("bridge has opened connection")
                        connection, address = self.server_socket.accept()
                        connection.setblocking(0)
                        # register on events from client
                        epoll.register(connection.fileno(), select.EPOLLIN)
                        connections[connection.fileno()] = connection
                        self.socket_output               = connection
                        # make out_epoll configured to write datas in send_message
                        # function
                        self.out_epoll.register(self.socket_output.fileno(),
                                                select.EPOLLOUT)
                        self.ready_send = True
                        self.sendLock.release()
                    elif event & select.EPOLLIN :
                        length = 0
                        # if we start a new message
                        if s_buffered_m == None :
                            data = bytearray()
                            # if we start a new header
                            if s_header_m == None :
                                data = self.socket_output.recv(4)
                            # otherwise complete the previous header
                            else :
                                data = s_header_m + self.socket_output.recv(s_rest_header)
                            # If we havent received enough to have a valid
                            # header, wait for the rest to come
                            if len(data) < 4 :
                                s_header_m    = data
                                s_rest_header = 4 - len(s_header_m)
                                length        = -1
                            # Otherwise, lets decode the rest of the message
                            else :
                                # unpack the packet length
                                length = struct.unpack_from('>i', data)[0]
                                s_header_m = None
                        else :
                            length = s_rest_to_read
                        if length > -1 :
                            # read what we're supposed to
                            data = self.socket_output.recv(length)
                            # if the received message is a full one, handle it,
                            # otherwise, store it for later
                            if len(data) == length :
                                if s_buffered_m != None :
                                    data = s_buffered_m + data
                                    s_buffered_m = None
                                self._handle_message(self.socket_output, data)
                            else :
                                s_rest_to_read = length - len(data)
                                if s_buffered_m == None :
                                    s_buffered_m = data
                                else :
                                    s_buffered_m = s_buffered_m + data
        except :
            raise
        finally:
            epoll.unregister(self.server_socket.fileno())
            epoll.close()
            self.server_socket.close()

    # data need to be str format assuming utf-8
    # Will be send using
    def send_message(self, data):
        if not self.ready_send :
            self.sendLock.acquire()
        to_send      = self.pack(data)
        rest_to_send = len(to_send)
        try :
            while rest_to_send > 0 and self.poll:
                events  = self.out_epoll.poll(consts.timeout)
                for fileno, event in events:
                    if event & select.EPOLLOUT :
                        byteswritten = self.socket_output. send(to_send)
                        if byteswritten < rest_to_send :
                            to_send      = to_send[byteswritten:]
                        rest_to_send = rest_to_send - byteswritten
        except:
            self.flushprint("ouch !")
            raise

    def ask_taktuk_infos(self) :
        self.send_message(consts.DEMAND_INFOS)

    # transform string to packed datas
    # data need to be str format assuming utf-8
    def pack(self, data):
        # send the packed data and the packed length
        data = bytes(data,consts.encoding)
        return struct.pack(">i", len(data)) + data

    # is called when a message arrive on the socket
    def _handle_message(self, socket, data):
        self.recv_callback(data.decode(consts.encoding))

    # stop the thread
    def stop_epoll(self):
        self.poll = False

    # kill the bridge and clean the socket file
    def shutdown(self):
        # ask taktuk to kill the bridge
        # ask taktuk to remove the control_adress file
        print("shutdown socket wrapper")
        self.taktuk.send_command("synchronize 0 kill target all")
        self.taktuk.send_command("synchronize 0 exec [ rm "+self.control_adress+" ]")
        self.stop_epoll()


