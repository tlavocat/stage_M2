#!/usr/bin/env python3.5
import sys
from isengard.isengardc import Helper

def main(argv):
    helper = Helper()
    print("node1")
    print(" ".join(helper.build_list("node1")) == "node1")
    print("\nnode[19]")
    print(" ".join(helper.build_list("node[19]")) == "node19")
    print("\nnode[1-3]")
    print(" ".join(helper.build_list("node[1-3]")) == "node1 node2 node3")
    print("\nnode[1-3],otherhost/node2")
    print(" ".join(helper.build_list("node[1-3],otherhost/node2")) == "node1 node3 otherhost")
    print("\nnode[1-3,5]part[a-b]/node[3-5]parta,node1partb")
    print(" ".join(helper.build_list("node[1-3,5]part[a-b]/node[3-5]parta,node1partb")) == "node1parta node2parta node2partb node3partb node5partb")
    print("\nA,B,C,D,E,F")
    print(" ".join(helper.build_list("A,B,C,D,E,F")) == "A B C D E F")

    print(helper.compose([1]))
    print(helper.compose([1,2,3,4]))
    print(helper.compose([1,4,5,10]))
    print(helper.compose([1,4,5,6,10,11,12]))

    print(helper.build_list("["+",".join(helper.compose([1]))+"]"))
    print(helper.build_list("["+",".join(helper.compose([1,2,3,4]))+"]"))
    print(helper.build_list("["+",".join(helper.compose([1,4,5,10]))+"]"))
    print(helper.build_list("["+",".join(helper.compose([1,4,5,6,10,11,12]))+"]"))

if __name__ == "__main__":
    sys.exit(main(sys.argv))
