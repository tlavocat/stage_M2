#!/usr/bin/env python3
import sys
from isengard import consts
from erebor   import main
from task_lib import Group
from task_lib import NumberedGroup
from task_lib import Broadcaster
from task_lib import SerialBroadcaster
from task_lib import ParrallelBroadcaster
from task_lib import Ventilator
from task_lib import TaskProcessor

class Sample(TaskProcessor):
    def __init__(self, erebor, ID, node_list, tfile=None):
        TaskProcessor.__init__(self, erebor, ID, tfile)

    def register_tasks(self) :
        g1 = Group("g1", "g1", "nonexistingcomputernowhere", "0", "root")
        g2 = Group("g2", "g2", "A,A,A,A", "0", "root")
        g2.add_dependency_conjunction({g1:consts.SICK},False)
        self.tasks.append(g1)
        self.tasks.append(g2)

if __name__ == "__main__":
    sys.exit(main(sys.argv, Sample))
