#!/usr/bin/env python3
import json
import getopt
import sys
import zmq
from isengard import consts
from erebor   import MPIDecoder

def main(argv):
    decoder = MPIDecoder()
    print("start")
    try:
        options, argv = getopt.getopt(
                argv[1:],
                "p:r:n:",
                [ "port=", "rank=", "network="]
                )
    except getopt.GetoptError as message:
        sys.stdout.write("toto")
    # parse options
    port    = ""
    rank    = ""
    network =""
    for option, value in options:
        if option in ['-p', '--port'] :
            port = value
        if option in ["-r", "--rank"]:
            rank = value;
        if option in ["-n", "--network"]:
            network = value;
    # create a zmq context
    context = zmq.Context()
    print("zmq context created")
    request = context.socket(zmq.PUSH)
    print("request socket created {}".format(port))
    # Connect to the MPI hook using zmq
    request.connect("ipc:///tmp/{}".format(port))
    print("connected")
    # Create a reply socket to be notified on incoming messages
    reply   = context.socket(zmq.PULL)
    reply.bind("ipc:///tmp/{}".format(int(port)+1000))
    print(str(int(port)+1000))
    # build re request message for the zmq server
    message = decoder.pack(consts.REG, rank, str(int(port)+1000))
    print("message to register {}".format(message))
    request.send(message)
    # ask to send a message to myself
    message = decoder.pack(
        consts.MPISEND,
        rank,
        network,
        rank,
        network,
        "coucou"
        )
    print(message)
    request.send(message)
    # wait for an answer
    message = decoder.unpack(reply.recv())
    print("received message {}".format(message))

if __name__ == "__main__":
    sys.exit(main(sys.argv))
