#!/usr/bin/env python3
import json
import getopt
import sys
import zmq
from isengard import consts
from erebor   import MPIDecoder

def main(argv):
    decoder = MPIDecoder()
    print("start")
    try:
        options, argv = getopt.getopt(
                argv[1:],
                "p:r:c:",
                [ "port=", "rank=", "clients="]
                )
    except getopt.GetoptError as message:
        sys.stdout.write("toto")
    # parse options
    port    = ""
    rank    = ""
    clients = 0
    for option, value in options:
        if option in ['-p', '--port'] :
            port = value
        if option in ["-r", "--rank"]:
            rank = value;
        if option in ["-c", "--clients"]:
            clients = int(value);
    # create a zmq context
    context = zmq.Context()
    print("zmq context created")
    request = context.socket(zmq.PUSH)
    print("request socket created {}".format(port))
    # Connect to the MPI hook using zmq
    request.connect("ipc:///tmp/{}".format(port))
    print("connected")
    # Create a reply socket to be notified on incoming messages
    reply   = context.socket(zmq.PULL)
    reply_p = int(port)+1000+int(rank)
    reply.bind("ipc:///tmp/{}".format(reply_p))
    print(reply_p)
    # build re request message for the zmq server
    message = decoder.pack(consts.REG, rank, str(reply_p))
    print(message)
    request.send(message)
    # wait for an answer
    for i in range(0, clients) :
        message = decoder.unpack(reply.recv())
        print("received message {}".format(message))
        node   = message[0]
        group  = message[1]
        # ask to send a message to server
        message = decoder.pack(
            consts.MPISEND,
            node,
            group,
            "1",
            "server",
            "coucou well received"
            )
        print(message)
        request.send(message)

if __name__ == "__main__":
    sys.exit(main(sys.argv))
