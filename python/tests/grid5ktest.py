#!/usr/bin/env python3
import traceback
import cProfile
import getopt
import time
import json
import sys
import signal
import threading
from erebor   import main
from erebor   import FrameworkControler
from erebor   import Erebor
from isengard import consts

def flushprint(*args, **kwargs):
    print(*args, file=sys.stdout, **kwargs)
    sys.stdout.flush()

class Grid5000Test(FrameworkControler):

    def __init__(self, erebor, ID, node_list, tfile):
        FrameworkControler.__init__(self, erebor, ID, tfile)
        self.node_list = node_list

    def start(self, networkId):
        flushprint("\n 'connected' \n")
        network = self.erebor.networks.get(networkId)
        self.log_time("connect")
        def dead_nodes(rank, ID, nodes) :
            flushprint("dead nodes [ from rank {} ID {}] {}".format(rank, ID, nodes))
        self.erebor.on_dead_nodes(dead_nodes)
        def network_root_up(error_nodes):
            flushprint("network connected")
            self.log_time("network up")
            def network_root_update(data):
                flushprint("network updated")
                self.log_time("network updated")
                def done(data) :
                    flushprint("done {}".format(data))
                    self.log_time("command executed")
                    self.erebor.terminate()
                    self.close()
                self.broadcast_exec_on("0", "uptime",consts.TRUE, "0", "root",
                                       "0", "root", done)
            self.network_update_on(consts.TRUE, "0", "root", "0", "root",
                    network_root_update)
        self.spawn_on("0",self.node_list, consts.FALSE, "0", "root", "0", "root",
                      network_root_up)

if __name__ == "__main__":
    sys.exit(main(sys.argv, Grid5000Test))
