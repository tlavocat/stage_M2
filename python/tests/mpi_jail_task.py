#!/usr/bin/env python3
import sys
import os
from isengard import consts
from erebor   import main
from task_lib import MPIJail
from task_lib import MPIExecutor
from task_lib import TaskProcessor

class Sample(TaskProcessor):
    def __init__(self, erebor, ID, node_list, tfile=None):
        TaskProcessor.__init__(self, erebor, ID, tfile)

    def register_tasks(self) :
        nb_clients = 10
        g1 = MPIJail("jail_server", 'server', '', '0', 'root')
        server_jail = MPIExecutor('s1', 
                os.environ["PWD"]+"/../c_wrapper/server -C {}".format(nb_clients),
                g1, True)
        server_jail.add_dependency_conjunction({g1:consts.RUNING}, True)
        self.tasks.append(g1)
        self.tasks.append(server_jail)
        for i in range(0, nb_clients) :
            gc = MPIJail("jail_client{}".format(i), "client{}".format(i), '', '0', 'root')
            client_jail = MPIExecutor("mpi_executor{}".format(i),
                            os.environ["PWD"]+"/../c_wrapper/client -R 1 -S {}".format("server"),
                            gc,True)
            client_jail.add_dependency_conjunction(
                    {gc:consts.RUNING,server_jail:consts.RUNING}, True)
            self.tasks.append(gc)
            self.tasks.append(client_jail)

if __name__ == "__main__":
    sys.exit(main(sys.argv, Sample))
