#!/usr/bin/env python3
import json
import getopt
import sys
import zmq
from isengard import consts
from erebor   import MPIDecoder

def main(argv):
    decoder = MPIDecoder()
    print("start")
    try:
        options, argv = getopt.getopt(
                argv[1:],
                "p:r:s:n:",
                [ "port=", "rank=", "server=", "network"]
                )
    except getopt.GetoptError as message:
        sys.stdout.write("toto")
    # parse options
    port    = ""
    rank    = ""
    server  = ""
    network = ""
    for option, value in options:
        if option in ['-p', '--port'] :
            port = value
        if option in ["-r", "--rank"]:
            rank = value;
        if option in ["-s", "--server"]:
            server = value;
        if option in ["-n", "--network"]:
            network = value;
    # create a zmq context
    context = zmq.Context()
    print("zmq context created")
    request = context.socket(zmq.PUSH)
    print("request socket created {}".format(port))
    # Connect to the MPI hook using zmq
    request.connect("ipc:///tmp/{}".format(port))
    print("connected")
    # Create a reply socket to be notified on incoming messages
    reply   = context.socket(zmq.PULL)
    reply_p = int(port)+1000+int(rank)
    reply.bind("ipc:///tmp/{}".format(reply_p))
    print(reply_p)
    # build re request message for the zmq server
    message = decoder.pack(consts.REG, rank, str(reply_p))
    print("message to register {}".format(message))
    request.send(message)
    # ask to send a message to server
    message = decoder.pack(
        consts.MPISEND,
        "1",
        server,
        rank,
        network,
        "coucou"
        )
    print(message)
    request.send(message)
    message = decoder.unpack(reply.recv())
    print("received message {}".format(message))
    print("job done for me {}".format(rank))

if __name__ == "__main__":
    sys.exit(main(sys.argv))
