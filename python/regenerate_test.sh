#!/bin/bash
time launcher tests/sample.py > regression/sample_base
time launcher tests/grid5ktest.py --host-list=A,A,A,A > regression/grid5ktest_base
time launcher tests/grid5kSlowGrouptest.py --host-list=A,A,A,A > regression/grid5kSlowGrouptest_base
time launcher tests/grid5kGrouptest.py --host-list=A,A,A,A > regression/grid5kGrouptest_base
time launcher tests/simple_mpi_communicator_test.py > regression/simple_mpi_base
time launcher tests/mpi_communicator_test.py > regression/mpi_base
time launcher tests/c_communicator_test.py > regression/c_communicator_test_base
