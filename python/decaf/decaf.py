#!/usr/bin/env python3
import sys
import os
from isengard import consts
from erebor   import main
from task_lib import MPIJail
from task_lib import SerialBroadcaster
from task_lib import MPIExecutor
from task_lib import TaskProcessor

base                = "/home/thomas/Documents/inria/decaf/build/tests/"
decaf_consumer_path = "mpirun -np 4 {}/testcountcci -n 4 -m 4 -s".format(base)
decag_producer_path = "mpirun -np 4 {}/testcountcci -n 4 -m 4 -c".format(base)

class Sample(TaskProcessor):
    def __init__(self, erebor, ID, node_list, tfile=None):
        TaskProcessor.__init__(self, erebor, ID, tfile)

    def register_tasks(self) :
        # Execute the consumer
        decaf_consumer_group    = MPIJail("server", 'server', 'A', '0', 'root')
        decaf_consumer_executor = MPIExecutor('s1',
                decaf_consumer_path,
                decaf_consumer_group,
                True)

        decaf_consumer_executor.add_dependency_conjunction(
                {decaf_consumer_group:consts.RUNING},
                True)

        self.tasks.append(decaf_consumer_group)
        self.tasks.append(decaf_consumer_executor)

        # Execute the producer
        decaf_producer_group    = MPIJail("client", "client", 'B', '0', 'root')
        decaf_producer_executor = MPIExecutor("p1",
                        decag_producer_path+" -S server ",
                        decaf_producer_group,
                        True)

        # Producer depends on its group running and its consumer running
        decaf_producer_executor.add_dependency_conjunction(
                {decaf_producer_group:consts.RUNING,decaf_consumer_executor:consts.RUNING},
                True)

        self.tasks.append(decaf_producer_group)
        self.tasks.append(decaf_producer_executor)

if __name__ == "__main__":
    sys.exit(main(sys.argv, Sample))
