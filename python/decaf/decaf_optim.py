#!/usr/bin/env python3
import sys
import os
from isengard import consts
from erebor   import main
from task_lib import Group
from task_lib import MPIJail
from task_lib import Barrier
from task_lib import SerialBroadcaster
from task_lib import MPIExecutor
from task_lib import TaskProcessor

base                = "/home/thomas/Documents/inria/decaf/build/tests/"
decaf_consumer_path = "mpirun -np 4 {}/testcountcci -n 4 -m 4 -s".format(base)
decag_producer_path = "mpirun -np 4 {}/testcountcci -n 4 -m 4 -c".format(base)

class Sample(TaskProcessor):
    def __init__(self, erebor, ID, node_list, tfile=None):
        TaskProcessor.__init__(self, erebor, ID, tfile)

    def register_tasks(self) :
        # Execute the consumer
        decaf_consumer_group    = MPIJail("server", 'server', 'A', '0', 'root')
        decaf_consumer_executor = MPIExecutor('consumer',
                decaf_consumer_path,
                decaf_consumer_group,
                True)
        # Need a different TakTuk instance to launch the MPI program
        # To be free to kill the MPIJail without impacting the usefull execution
        shadow_1 = Group("shadow1", 'shadow1', '', '0', 'root')
        # the decaf consumer will be launched by the shadow group
        decaf_consumer_executor.group = shadow_1

        # The group will be ended  by the barrier, reducing footprint over the
        # simulation
        server_barrier = Barrier("sb1", "sbarrier",
                                 decaf_consumer_group, "root", 1)
        server_barrier.add_dependency_conjunction(
                {decaf_consumer_group:consts.RUNING, shadow_1:consts.RUNING},
                True)
        # The server code is started upon barrier bootstrap
        decaf_consumer_executor.add_dependency_conjunction(
                {server_barrier:consts.RUNING},
                True)

        self.tasks.append(decaf_consumer_group)
        self.tasks.append(shadow_1)
        self.tasks.append(server_barrier)
        self.tasks.append(decaf_consumer_executor)

        # Execute the producer
        decaf_producer_group    = MPIJail("client", "client", 'B', '0', 'root')
        decaf_producer_executor = MPIExecutor("p1",
                        decag_producer_path+" -S server ",
                        decaf_producer_group,
                        True)
        # Need a different TakTuk instance to launch the MPI program
        # To be free to kill the MPIJail without impacting the usefull execution
        shadow_2 = Group("shadow2", 'shadow2', '', '0', 'root')
        # the decaf producer is launched by the shadow group
        decaf_producer_executor.group = shadow_2

        # The group will be ended  by the barrier, reducing footprint over the
        # simulation
        client_barrier = Barrier("cb1", "cbarrier",
                                 decaf_producer_group, "root", 4)
        client_barrier.add_dependency_conjunction(
                {decaf_producer_group:consts.RUNING,
                 shadow_2:consts.RUNING},
                True)

        # Producer depends on its group running and its consumer running
        decaf_producer_executor.add_dependency_conjunction(
                {client_barrier:consts.RUNING,
                decaf_consumer_executor:consts.RUNING},
                True)

        self.tasks.append(decaf_producer_group)
        self.tasks.append(shadow_2)
        self.tasks.append(client_barrier)
        self.tasks.append(decaf_producer_executor)

if __name__ == "__main__":
    sys.exit(main(sys.argv, Sample))
