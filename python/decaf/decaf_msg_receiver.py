#!/usr/bin/env python3
import sys
import os
from isengard import consts
from erebor   import main
from task_lib import MPIJail
from task_lib import SerialBroadcaster
from task_lib import MPIExecutor
from task_lib import TaskProcessor
from task_lib import MessageReceiver

base                = "/home/thomas/Documents/inria/decaf/build/tests/"
decaf_consumer_path = "mpirun -np 4 {}/testcountcci -n 4 -m 4 -s".format(base)
decag_producer_path = "mpirun -np 4 {}/testcountcci -n 4 -m 4 -c".format(base)

class Sample(TaskProcessor):
    nb_coucou = 1;

    def __init__(self, erebor, ID, node_list, tfile=None):
        TaskProcessor.__init__(self, erebor, ID, tfile)

    def ctrl_msg(self, data):
        print("message : "+data + " | received {}".format(self.nb_coucou))
        if self.nb_coucou % 5 == 0 :
            for i in range(0, 4) :
                self.mpi_tnet_msg_on("Yop", str(i), "client", "0", "root",
                "0", "root", "0", "root");
        self.nb_coucou+=1
        return self.nb_coucou == 21

    def register_tasks(self) :
        messager = MessageReceiver("msg", "coucou.*", self.ctrl_msg, "root",10)
        self.tasks.append(messager);

        # Execute the consumer
        decaf_consumer_group    = MPIJail("server", 'server', 'A', '0', 'root')
        decaf_consumer_executor = MPIExecutor('s1',
                decaf_consumer_path,
                decaf_consumer_group,
                True)

        decaf_consumer_executor.add_dependency_conjunction(
                {decaf_consumer_group:consts.RUNING},
                True)

        self.tasks.append(decaf_consumer_group)
        self.tasks.append(decaf_consumer_executor)

        # Execute the producer
        decaf_producer_group    = MPIJail("client", "client", 'B', '0', 'root')
        decaf_producer_executor = MPIExecutor("p1",
                        decag_producer_path+" -S server ",
                        decaf_producer_group,
                        True)

        # Producer depends on its group running and its consumer running
        decaf_producer_executor.add_dependency_conjunction(
                {decaf_producer_group:consts.RUNING,decaf_consumer_executor:consts.RUNING},
                True)

        self.tasks.append(decaf_producer_group)
        self.tasks.append(decaf_producer_executor)

if __name__ == "__main__":
    sys.exit(main(sys.argv, Sample))
