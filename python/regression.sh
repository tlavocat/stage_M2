#!/bin/bash

echo
echo -- Testing packing :

if ./launcher ./erebor/encoder.py
then
    echo packing OK
else
    echo packing NOK
    exit 1
fi

echo
echo -- Testing failing groups :
time launcher tests/fail_group.py > regression/fail_group_test
echo
if diff  <(sort regression/fail_group_base) <(sort regression/fail_group_base)
then
    echo fail OK
    rm regression/fail_group_test
else
    echo fail NOK
    exit 1
fi

echo
echo -- Testing basic task processor :
time ./launcher tests/simple_task.py > regression/simple_task_test
echo
if diff <(sed "/.*[B|A]-/d" <(sort regression/simple_task_base)) <(sed "/.*[B|A]-/d" <(sort regression/simple_task_test))
then
    echo task processor OK
    rm regression/simple_task_test
else
    echo task processor NOK
    exit 1
fi

echo
echo -- Testing mpi jail:
time ./launcher tests/mpi_jail_task.py > regression/mpi_jail_task_test
echo
if diff <(sed "/status/d" <(sort regression/mpi_jail_task_base)) <(sed "/status/d" <(sort regression/mpi_jail_task_test))
then
    echo task processor OK
    rm regression/mpi_jail_task_test
else
    echo task processor NOK
    exit 1
fi

echo
echo -- Testing basic sample :
time ./launcher tests/sample.py > regression/sample_test
echo
if diff <(sed "/uptime/d" <(sort regression/sample_base)) <(sed "/uptime/d" <(sort regression/sample_test))
then
    echo sample OK
    rm regression/sample_test
else
    echo sample NOK
    exit 1
fi
echo

echo
echo -- Testing broadcast :
time ./launcher tests/grid5ktest.py --host-list=A,A,A,A > regression/grid5ktest_test
echo
if diff <(sed "/done/d" <(sort regression/grid5ktest_test)) <(sed "/done/d" <(sort regression/grid5ktest_base))
then
    echo Broadcast OK
    rm regression/grid5ktest_test
else
    echo Broadcast NOK
    exit 1
fi
echo

echo
echo -- Testing replication :
time ./launcher tests/grid5kSlowGrouptest.py --host-list=A,A,A,A > regression/grid5kSlowGrouptest_test
echo
if diff <( sed '/{.*}/d' <(sed "/to wait/d" <(sort regression/grid5kSlowGrouptest_test))) <( sed '/{.*}/d' <(sed "/to wait/d" <(sort regression/grid5kSlowGrouptest_base)))
then
    echo Replication OK
    rm regression/grid5kSlowGrouptest_test
else
    echo Replication NOK
    exit 1
fi
echo

echo
echo -- Testing replication and routing :
time ./launcher tests/grid5kGrouptest.py --host-list=A,A,A,A > regression/grid5kGrouptest_test
echo
if diff <( sed '/{.*}/d' <(sed "/to wait/d" <(sort regression/grid5kGrouptest_test))) <( sed '/{.*}/d' <(sed "/to wait/d" <(sort regression/grid5kGrouptest_base)))
then
    echo Replication and routing OK
    rm regression/grid5kGrouptest_test
else
    echo Replication and routing NOK
    exit 1
fi
echo

echo
echo -- Testing simple emulated mpi communications :
time ./launcher tests/simple_mpi_communicator_test.py > regression/simple_mpi_test
echo
if diff <(sed "/with status/d" <(sort regression/simple_mpi_test)) <(sed "/with status/d" <(sort regression/simple_mpi_base))
then
    echo SIMPLE MPI OK
    rm regression/simple_mpi_test
else
    echo SIMPLE MPI NOK
    exit 1
fi
echo

echo
echo -- Testing emulated mpi communications :
time ./launcher tests/mpi_communicator_test.py > regression/mpi_test
echo
if diff <(sed "/with status/d" <(sort regression/mpi_test))  <(sed "/with status/d" <(sort regression/mpi_base))
then
    echo MPI OK
    rm regression/mpi_test
else
    echo MPI NOK
    exit 1
fi

echo
echo -- Testing emulated mpi communications with C programms:
time ./launcher tests/c_communicator_test.py > regression/c_communicator_test_test
echo
if diff <(sed "/with status/d" <(sort regression/c_communicator_test_test))  <(sed "/with status/d" <(sort regression/c_communicator_test_base))
then
    echo C OK
    rm regression/c_communicator_test_test
else
    echo C NOK
    exit 1
fi

