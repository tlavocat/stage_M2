import sys
import socket
import time
import re
import signal
import json
from isengard           import flushp
from isengard           import consts
from isengard.isengardc import Isengard
from isengard.consts    import bcolors

class Network(Isengard) :

    def __init__(self, add_to_queue, root, ID, debug_list, taktuk_path,
                                                    options="", log_time=None):
        Isengard.__init__(self, add_to_queue, root, debug_list, taktuk_path,
                                                              options, log_time)
        self.debug_list      = debug_list
        self.debug           = "network" in self.debug_list
        if self.debug :
            self.flushprint      = flushp.flushprint
        else :
            self.flushprint      = flushp.flushprint_nothing
        self.ID = ID
        self.child_callbacks = []
        self.order_callbacks = []
        self.ntwrk_callbacks = []
        self.mtrsn_callbacks = []
        self.trsnt_callbacks = []
        self.sbprc_callbacks = []
        self.stats_callbacks = []
        self.spawn_callbacks = []
        self.lost_callbacks  = []
        self.dead_callbacks  = []
        self.ntwud_callbacks = []
        self.ntwrn_callbacks = []
        self.deadb_callbacks = []
        self.gmsg_callbacks  = []
        self.state           = consts.INIT

    # Registration ############################################################

    def one_shot_register_on_network_update(self, values):
        self.ntwud_callbacks.append(values)

    def one_shot_register_on_network_renumber(self, values):
        self.ntwrn_callbacks.append(values)

    def register_on_dead_bridge(self, values):
        self.deadb_callbacks.append(values)

    def register_on_bridge_generic_messages(self, values):
        self.gmsg_callbacks.append(values)

    def register_on_lost_nodes(self, values):
        self.lost_callbacks.append(values)

    def register_on_dead_nodes(self, values):
        self.dead_callbacks.append(values)

    def one_shot_register_on_spawn_nodes(self, values):
        self.spawn_callbacks.append(values)

    def one_shot_register_on_status_print(self, values):
        self.stats_callbacks.append(values)

    def register_on_subprocess_callback(self, register_callback):
        self.sbprc_callbacks.append(register_callback)

    def register_mpi_trans_network_callback(self, register_callback):
        self.mtrsn_callbacks.append(register_callback)

    def register_trans_network_callback(self, register_callback):
        self.trsnt_callbacks.append(register_callback)

    def register_network_bootstrap_callback(self, register_callback):
        self.ntwrk_callbacks.append(register_callback)

    def register_order_callback(self, register_callback):
        self.order_callbacks.append(register_callback)

    def register_children_callback(self, register_callback):
        self.child_callbacks.append(register_callback)

    # Events ###################################################################

    # When an order pop
    def new_mpi_trans_network_has_come(self, order, rank):
        self.call_mpi_transnet_callbacks(order, rank)

    # When an order pop
    def new_trans_network_has_come(self, order, rank):
        self.call_transnet_callbacks(order, rank)

    # When an order pop
    def new_order_has_come(self, order, rank):
        self.call_order_callbacks(order, rank, self.ID)

    # When a child pop
    def new_child_has_come(self, rank, ID):
        self.call_connection_callbacks(rank, ID)

    # is called when a subprocess write something
    def _subprocess_callback(self, txt) :
        if len(self.sbprc_callbacks) == 0 :
            self.flushprint(bcolors.WARNING+ txt +bcolors.ENDC)
        else :
            self.call_sbprc_callbacks(self.ID, txt)
        return json.dumps({consts.TYPE:consts.CONTROL, consts.VALUE:consts.ACK})

    def bridge_dead(self) :
        self.call_dead_bridge_calllbacks()

    def bridge_generic_message_callback(self, txt) :
        self.flushprint("receive generic bridge message {}".format(txt))
        self.call_gmsg_callbacks(txt)

    # is called when something arrive from the bridge
    def bridge_internal_callback(self, obj) :
        if obj[consts.TYPE] == consts.INFOS :
            # Look for rank information
            self.rank = obj[consts.RANK]
            #flushprint("my rank is "+self.rank)
            # If not network_master, forward the information
            self.notify_bootstrap(self.rank != "0")
        elif obj[consts.TYPE] == consts.QUIT :
            self.new_order_has_come(json.loads(consts.QUIT_ORDER), "0")
        # Display some logs
        else :
            self.flushprint(bcolors.FAIL+ "-> Bridge log : "+obj[consts.DATA]+bcolors.ENDC)

    def bridge_isengard_callback(self, sender, decoded_data) :
        if decoded_data[consts.TYPE] == consts.INFOS :
            # Get children's info
            # It means a new one as shown
            # transfert request
            # TODO get target info
            self.new_child_has_come(
                    decoded_data[consts.RANK],
                    decoded_data[consts.ID])
        # If it is an order
        elif decoded_data[consts.TYPE] == consts.ORDER :
            self.new_order_has_come(decoded_data, sender)
        # If it is a trans-network communication
        elif decoded_data[consts.TYPE] == consts.TNETWRK :
            self.new_trans_network_has_come(decoded_data, sender)
        elif decoded_data[consts.TYPE] == consts.MTNETWRK :
            self.new_mpi_trans_network_has_come(decoded_data, sender)

    # is called when something arrive from taktuk output
    def taktuk_stdout_callback(self, txt) :
        self.flushprint(bcolors.OKBLUE+ txt +bcolors.ENDC)

    # is called when status information are displayed by taktuk
    def _status_print(self, txt):
        self.flushprint(bcolors.WHITE+bcolors.UNDERLINE+ txt +bcolors.ENDC)
        if txt.startswith(socket.gethostname()) :
            self.call_stats_callbacks()

    def fire_connection_lost(self, sp_node, error_nodes) :
        self.call_lost_callbacks(sp_node, error_nodes)

    def fire_node_dead(self, sp_node, error_nodes, errors_rank) :
        self.call_dead_callbacks(error_nodes, errors_rank)

    def fire_network_ready_callback(self, error_nodes):
        self.call_spawn_callbacks(",".join(error_nodes))

    def fire_network_renumber_callback(self):
        self.call_ntwrn_callbacks()

    def fire_network_update_failure_callback(self):
        self.call_ntwud_callbacks("failure")

    def fire_network_update_success_callback(self):
        self.call_ntwud_callbacks("success")

    def kill_needed(self, n):
        self.new_order_has_come(json.loads(consts.QUIT_ORDER), "0")
        sys.exit(-1)

    # Propagation ##############################################################

    # Notify my bootstrap :
    # -> to the registered callbacks
    # -> to my network master (if it exsit : propagate = true)
    def notify_bootstrap(self, propagate):
        self.state = consts.RUNING
        data  = json.dumps({
            consts.FROM:self.rank,
            consts.TYPE:consts.INFOS,
            consts.RANK:self.rank,
            consts.ID:self.ID
            });
        if propagate :
            self.send_message_to("0", "all", data, False)
        self.call_ntwrk_callbacks(data)

    def call_sbprc_callbacks(self, ID, txt) :
        for callback in self.sbprc_callbacks :
            callback(ID, txt)

    def call_ntwrk_callbacks(self, order) :
        for callback in self.ntwrk_callbacks :
            callback(order)

    def call_order_callbacks(self, order,rank,ID) :
        for callback in self.order_callbacks :
            callback(order, rank, ID)

    def call_mpi_transnet_callbacks(self, order, rank) :
        for callback in self.mtrsn_callbacks:
            callback(order, rank)

    def call_transnet_callbacks(self, order, rank) :
        for callback in self.trsnt_callbacks:
            callback(order, rank)

    def call_connection_callbacks(self, rank, ID) :
        for callback in self.child_callbacks :
            callback(rank, ID)

    def call_stats_callbacks(self) :
        for c in self.stats_callbacks :
            c.callback("", c.dnode, c.dnetid, c.snode, c.snetid, c.value)
        self.stats_callbacks = []

    def call_ntwud_callbacks(self, failure) :
        for c in self.ntwud_callbacks :
            c.callback(failure, c.dnode, c.dnetid, c.snode, c.snetid, c.value)
        self.ntwud_callbacks = []

    def call_ntwrn_callbacks(self) :
        for c in self.ntwrn_callbacks :
            c.callback("", c.dnode, c.dnetid, c.snode, c.snetid, c.value)
        self.ntwrn_callbacks = []

    def call_dead_bridge_calllbacks(self) :
        for c in self.deadb_callbacks :
            c(self)

    def call_lost_callbacks(self, error_nodes, errors_rank) :
        for c in self.lost_callbacks :
            c(self.rank, self.ID, error_nodes, errors_rank)

    def call_dead_callbacks(self, error_nodes, errors_rank) :
        for c in self.dead_callbacks :
            c(self.rank, self.ID, error_nodes, errors_rank)

    def call_spawn_callbacks(self, error_nodes) :
        for c in self.spawn_callbacks :
            c.callback(error_nodes, c.dnode, c.dnetid, c.snode, c.snetid, c.value)
        self.spawn_callbacks = []

    def call_gmsg_callbacks(self, txt) :
        for c in self.gmsg_callbacks :
            c(txt)

    # Commands to manipulate Taktuk ############################################

    def network_update(self, synch, taktuk=True, callback_object=None) :
        # get notified on spawn
        if callback_object != None :
            self.one_shot_register_on_network_update(callback_object)
        Isengard.network_update(self, synch, taktuk)

    def network_renumber(self, synch, taktuk=True, callback_object=None) :
        # get notified on spawn
        if callback_object != None :
            self.one_shot_register_on_network_renumber(callback_object)
        Isengard.network_renumber(self, synch, taktuk)

    def spawn_nodes(self, dest, nodes, synch, taktuk=True, callback_object=None):
        # get notified on spawn
        if callback_object != None :
            self.one_shot_register_on_spawn_nodes(callback_object)
        Isengard.spawn_nodes(self, dest, nodes, synch, taktuk)

    def request_network_status(self, synch, taktuk=True, callback_object=None):
        # get notified on status request ?
        if callback_object != None :
            self.one_shot_register_on_status_print(callback_object)
        Isengard.request_network_status(self, synch, taktuk)

