#!/usr/bin/env python3
import traceback
import cProfile
import getopt
import time
import json
import sys
import signal
import threading
from erebor   import FrameworkControler
from erebor   import Erebor
from isengard import consts

def flushprint(*args, **kwargs):
    print(*args, file=sys.stdout, **kwargs)
    sys.stdout.flush()

def main(argv, Controler=None):
    flushprint("start")
    try:
        options, argv = getopt.getopt(
                argv[1:],
                "p:P:dDt:f:h:d:i:n",
                [ "erebor-path=", "taktuk-path=",
                  "erebor-deploy", "taktuk-deploy",
                  "time-file=", "file-host=","host-list=",
                  "debug=", "id=", "non-root"  ]
                )
    except getopt.GetoptError as message:
        sys.stdout.write("toto")

    try:
        root = True     # is root or not
        ID   = "root"   # group id to launch
        log  = []       # log life
        hlist= []       # host list
        tfile= None     # time-logger file
        epath= ""       # path to erebor (if not in the system path)
        tpath= ""       # path to taktuk (if not in the system path)
        deple= False    # erebor need to deploy itself
        deplt= False    # taktuk need to deploy itself
        for option, value in options:
            if option in ['t', '--time-file'] :
                tfile = value
            if option in ["-n", "--non-root"]:
                root = False;
            if option in ["-i", "--id"]:
                ID = value
            if option in ["-d", "--debug"]:
                if value == "*" :
                    log = ["erebor", "network", "isengard", "wrapper",
                    "unix_socket", "bridge", "mpi"]
                else :
                    log = value.split(",")
            if option in ["-h", "--host-list"]:
                hlist = value
            if option in ["-f", "--file-host"]:
                f = open(value, 'r')
                hlist = ",".join(f.readlines())
                hlist = hlist.replace("\n", "")
                hlist = hlist.replace("\r", "")
            if option in ["-p", "--erebor-path"]:
                epath = value
                if not epath.endswith("/") :
                    epath = epath+"/"
            if option in ["-P", "--taktuk-path"]:
                tpath = value
                if not tpath.endswith("/") :
                    tpath = tpath+"/"
            if option in ["-p", "--erebor-deploy"]:
                deple = True
            if option in ["-p", "--taktuk-deploy"]:
                deplt = True

        erebor = Erebor(root, log, epath, tpath)

        def signal_handler(a, b):
            erebor.terminate()

        signal.signal(signal.SIGINT, signal_handler)

        if root :
            controler = Controler(erebor, ID, hlist, tfile)
            # Start the test sample after the root network bootstrap
            erebor.on_network_init(ID, controler.start)
            controler.bootstrap()
        else :
            erebor.bootstrap(ID)
        erebor.process_messages()
    except:
        flushprint("Unexpected error: {}".format())
        #traceback.print_exception(sys.exc_info())
        erebor.terminate()
        sys.exit(-1)
if __name__ == "__main__":
    sys.exit(main(sys.argv))
