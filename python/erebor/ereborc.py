import math
import traceback
import os
import select
import pwd
import sys
import time
import re
import signal
import json
import threading
import queue
import multiprocessing
from threading import Lock
from threading import RLock
from isengard  import flushp
from isengard  import consts
from isengard  import Helper
from network   import Network
from rwlock    import RWLock
from consts    import bcolors
from mpibridge import MPIHook

class CallbackObject:
    def __init__(self, dnode, dnetid, snode, snetid, callback, value):
        self.dnode    = dnode
        self.dnetid   = dnetid
        self.snode    = snode
        self.snetid   = snetid
        self.value    = value
        self.callback = callback

def print_threading():
    for th in threading.enumerate():
        print(th)
        traceback.print_stack(sys._current_frames()[th.ident])
        print()

class RunningCommand:#TODO integrate node name
    def __init__(self, name, dest, command, debug,
            snode=None, snetid=None, dnode=None, dnetid=None,
            callback_on_terminaison=None):
        self.name    = name
        self.dest    = dest
        self.command = command
        self.stdout  = []
        self.stderr  = []
        self.finall  = ""
        self.debug   = debug
        self.wait    = 1
        if self.debug :
            self.flushprint = flushp.flushprint
        else :
            self.flushprint = flushp.flushprint_nothing
        self.stdout_regex = self.get_stdout_regex()
        self.stderr_regex = self.get_stderr_regex()
        self.final_regex  = self.get_final_regex()
        self.dnode        = dnode
        self.dnetid       = dnetid
        self.snode        = snode
        self.snetid       = snetid
        self.final_cllbck = callback_on_terminaison
        self.final_cllbck_value = -1

    def test_and_dispatch(self, txt):
        if self.stdout_regex.match(txt) != None :
            self.append_stdout(txt)
            return 0
        elif self.stderr_regex.match(txt) != None :
            self.append_stderr(txt)
            return 1
        elif self.final_regex.match(txt) != None :
            self.final(txt)
            if self.wait == 0 :
                return 2
            else :
                return 0
        else :
            return -1

    def get_stdout_regex(self):
        regex = "^.*{}-{}: {} \(.*\): output >.*".format(self.name, self.dest, self.command)
        self.flushprint("regex -> ^.*{}-{}: {} \(.*\): output >.*".format(self.name, self.dest, self.command))
        return re.compile(regex)
    def get_stderr_regex(self):
        regex = "^.*{}-{}: {} \(.*\): error >.*".format(self.name, self.dest, self.command)
        self.flushprint("regex -> ^.*{}-{}: {} \(.*\): error >.*".format(self.name, self.dest, self.command))
        return re.compile(regex)
    def get_final_regex(self):
        regex = "^.*{}-{}: {} \(.*\): status >.*".format(self.name, self.dest, self.command)
        self.flushprint("regex -> ^.*{}-{}: {} \(.*\): status >.*".format(self.name, self.dest, self.command))
        return re.compile(regex)

    def append_stdout(self, line):
        self.stdout.append(line)

    def append_stderr(self, line):
        self.stderr.append(line)

    # handle here ?
    def node_deads(self, error_nodes, errors_rank) :
        self.flushprint("error nodes {}".format(errors_rank))
        for i in range(0, len(error_nodes)) :
            node = error_nodes[i]
            rank = "X"
            if i < len(errors_rank) :
                rank = errors_rank[i]
            self.flushprint("comp '{}' '{}'".format(rank, self.dest))
            if self.dest == "[0-9]*" or rank in self.dest :
                self.flushprint("dead")
                self.final("dead")
                if self.wait == 0 :
                    return 2
                else :
                    return 1
        return 0

    def final(self, line):
        self.wait -= 1
        self.finall = line
        self.flushprint(" TO WARN {} {} {} {} {} ".format(self.snode, self.snetid, self.dnode,
            self.dnetid,self.final_cllbck_value))
        if self.final_cllbck != -1 :
            if self.snode != None :
                self.final_cllbck(self.stdout, self.stderr, self.finall,
                        self.snode, self.snetid, self.dnode, self.dnetid,
                        self.final_cllbck_value)
            else :
                self.final_cllbck(self.stdout, self.stderr, self.finall)

    def print_self(self, stdout, stderr, status):
        for s in stdout :
            self.flushprint(bcolors.OKGREEN+"COMMAND output > "+s+bcolors.ENDC)
        for s in stderr :
            self.flushprint(bcolors.OKGREEN+"COMMAND error > "+s+bcolors.ENDC)
        self.flushprint(bcolors.OKGREEN+"COMMAND status > "+status+bcolors.ENDC)

# TODO Handle the case where a node crash
class BrodcastRunningCommand(RunningCommand):
    def __init__(self, dest_list, command, debug, snode, snetid, dnode, dnetid,
                                                callback_on_terminaison, value):
        RunningCommand.__init__(self, "", "[0-9]*", command, debug, snode, snetid,
                dnode, dnetid, self.done)
        self.wait      = len(dest_list) -1
        self.flushprint("WAIT      {}".format(self.wait))
        self.end_out   = []
        self.my_final_cllbck       = callback_on_terminaison
        self.my_final_cllbck_value = value
        self.bstatus  = dict()
        self.bstdout  = dict()
        self.bstderr  = dict()
        self.flushprint("dest to wait broadcast command {}".format(self.wait))

    def append_stdout(self, line):
        identity = line.split(":")[0]
        if identity not in self.bstdout :
            self.bstdout[identity] = []
        self.bstdout[identity].append(line)

    def append_stderr(self, line):
        identity = line.split(":")[0]
        if identity not in self.bstderr :
            self.bstderr[identity] = []
        self.bstderr[identity].append(line)

    def append_status(self, line) :
        identity = line.split(":")[0]
        if identity not in self.bstatus :
            self.bstatus[identity] = []
        self.bstatus[identity].append(line)

    def final(self, line):
        self.wait -= 1
        self.append_status(line)
        if self.final_cllbck != -1 :
            if self.snode != None :
                self.final_cllbck(self.snode, self.snetid, self.dnode, self.dnetid,
                        self.final_cllbck_value)
            else :
                self.final_cllbck(self.stdout, self.stderr, self.finall)

    def done(self, snode, snetid, dnode, dnetid, dest):
        if self.wait == 0 :
            final = json.dumps({
                consts.STDOUT:self.bstdout,
                consts.STDERR:self.bstderr,
                consts.STATUS:self.bstatus
                })
            self.my_final_cllbck(final,
                    self.snode, self.snetid, self.dnode, self.dnetid,
                    self.my_final_cllbck_value)


class Neighbour :
    def __init__(self, rank, networkId):
        self.rank      = rank
        self.networkId = networkId
        self.target    = "all" #TODO
    def to_string(self) :
        return "{}@{} target {}".format(
                self.rank, self.networkId, self.target)

class SafeDict :
    def __init__(self):
        self.dict   = dict()

    def add(self, key, value):
        self.dict[key] = value

    def remove(self, key):
        value = self.get(key)
        del self.dict[key]
        return value

    def get(self, key):
        toreturn = None
        if key in self.dict.keys() :
            toreturn =  self.dict[key]
        return toreturn

    def get_copy(self):
        return self.dict

    def length(self):
        l =  len(self.dict.values())
        return l

# An Erebor is a network controler
# it can handle several networks
class Erebor:

    def __init__(self, root, debug_list, e_path, t_path, log_time=None):
        self.message_queue  = queue.Queue()
        self.erebor_path    = e_path
        self.taktuk_path    = t_path
        self.debug_list     = debug_list
        self.helper         = Helper()
        self.flog_time      = log_time
        self.debug          = "erebor" in self.debug_list
        if self.debug :
            self.flushprint = flushp.flushprint
        else :
            self.flushprint = flushp.flushprint_nothing
        self.state          = consts.INIT
        self.isRoot         = root
        self.run            = True
        # map networkId network
        self.networks       = SafeDict()
        # holder for master network
        self.master_network = None
        self.master_neighbr = None
        # rank@networkId Neighbour map.
        # Is populated when a child is connected on one of our master networks
        self.erebor_children=SafeDict()
        # dict string - callback
        self.ntwk_up_cllbks =dict()
        # dict string - callback
        self.ntwk_dn_cllbks =dict()
        # dict string - callback
        self.chld_btstrp_clb=dict()
        # Bind a network to a Neighbour gateway
        self.routing_table  =dict()
        # dead nodes callback
        self.dead_nodes_clv =[]
        #Take the end lock, only release it when terminate
        self.end_lock = Lock()
        self.end_lock.acquire()
        # network terminaison
        self.network_terminaison = dict()
        # running commands
        self.running_commands = []
        self.execute_callbacks=dict()
        self.execute_clb_count= 0
        # MPI
        self.mpi_routing_table      = dict()
        self.mpi_launching_to_wait  = dict()
        self.mpi_launching_callback = dict()
        self.mpi_bridges            = dict()
        self.mpi_wait_route_msg     = dict()

    def log_time(self, t):
        if self.flog_time !=  None :
            self.flog_time(t)

    # Registration #############################################################

    def register_execute_callback(self, register_callback):
        value = self.execute_clb_count
        self.execute_callbacks[value] = register_callback;
        self.execute_clb_count += 1
        return value

    # Routing ##################################################################

    # Send a trans network message by using the routing table
    # TODO
    def send_mpi_trans_network_message_to(self, mpi_dnode, dnetowkrId, mpi_snode,
                                                                snetworkId, m):
        self.flushprint("send mpi trans_network")
        gateway_network   = None
        neighbour_gateway = None
        # if we have a gateway to send the message
        if dnetowkrId in self.routing_table.keys() :
            neighbour_gateway = self.routing_table[dnetowkrId]
            gateway_network   = self.networks.get(neighbour_gateway.networkId)
            # If we do not know the route, send to the master he'll know what to
            # do.
        else :
            # otherwise take the default route
            self.flushprint("take master network to send the message")
            gateway_network   = self.master_network
            neighbour_gateway = self.master_neighbr
        if gateway_network != None :
            message = json.dumps({
                consts.TYPE:consts.MTNETWRK,
                consts.FROM:mpi_snode+"@"+snetworkId,
                consts.DEST:mpi_dnode+"@"+dnetowkrId,
                consts.DATA:m})
            self.flushprint("{} {}".format(neighbour_gateway.networkId,
                dnetowkrId))
            # if we are on the same network, send directly to the dnode
            # otherwise, send to the gateway rank
            if neighbour_gateway.networkId == dnetowkrId :
                # get the corresponding network to find the node to send
                network = self.networks.get(dnetowkrId)
                # find the taktuk_dnode corresponding to mpi_dnode
                self.flushprint(self.mpi_routing_table[network].keys())
                # if i'm the root of this network i can take a decision about
                # where to send the message. Otherwise I need to send it to the
                # root of the group, he'll know what to do.
                if network.rank == "0" :
                    self.flushprint("i'm rank 0 i can take a decision")
                    if mpi_dnode in self.mpi_routing_table[network] :
                        dnode = self.mpi_routing_table[network][mpi_dnode]
                        self.flushprint("MPI TRANS-NETWORK for mpi{}@{}, gateway {}@{}".format(
                                            mpi_dnode, dnetowkrId,
                                            dnode,
                                            neighbour_gateway.networkId))
                        gateway_network.send_message_to(
                                dnode,
                                neighbour_gateway.target,
                                message,
                                False)
                    else :
                        self.flushprint("need to buffer the message for later")
                        if mpi_dnode not in self.mpi_wait_route_msg :
                            self.mpi_wait_route_msg[mpi_dnode] = list()
                        # buffer the message for later
                        self.mpi_wait_route_msg[mpi_dnode].append([
                                            mpi_dnode, dnetowkrId, mpi_snode,
                                            snetworkId, m])
                else :
                    self.flushprint("forwarding to rank 0")
                    gateway_network.send_message_to(
                            "0",
                            neighbour_gateway.target,
                            message,
                            False)
            else :
                self.flushprint(
                          "MPI TRANS-NETWORK for {}@{}, gateway {}@{}".format(
                        mpi_dnode, dnetowkrId,
                        neighbour_gateway.rank,
                        neighbour_gateway.networkId))
                gateway_network.send_message_to(
                        neighbour_gateway.rank,
                        neighbour_gateway.target,
                        message,
                        False)
        else:
            self.flushprint("destination unknown")

    # Send a trans network message by using the routing table
    # Thread safe
    def send_trans_network_message_to(self, dnode, dnetowkrId, snode,
                                                                snetworkId, m):
        gateway_network   = None
        neighbour_gateway = None
        # if we have a gateway to send the message
        if dnetowkrId in self.routing_table.keys() :
            neighbour_gateway = self.routing_table[dnetowkrId]
            gateway_network = self.networks.get(neighbour_gateway.networkId)
            # If we do not know the route, send to the master he'll know what to
            # do.
        else :
            # otherwise take the default route
            self.flushprint("take master network to send the message")
            gateway_network   = self.master_network
            neighbour_gateway = self.master_neighbr
        if gateway_network != None :
            message = json.dumps({
                consts.TYPE:consts.TNETWRK,
                consts.FROM:snode+"@"+snetworkId,
                consts.DEST:dnode+"@"+dnetowkrId,
                consts.DATA:m})
            # if we are on the same network, send directly to the dnode
            # otherwise, send to the gateway rank
            if neighbour_gateway.networkId == dnetowkrId :
                self.flushprint(
                          " TRANS-NETWORK for {}@{}, gateway {}@{}".format(
                        dnode, dnetowkrId,
                        dnode,
                        neighbour_gateway.networkId))
                gateway_network.send_message_to(
                        dnode,
                        neighbour_gateway.target,#TODO retrieve target
                        message,
                        False)
            else :
                self.flushprint(
                          " TRANS-NETWORK for {}@{}, gateway {}@{}".format(
                        dnode, dnetowkrId,
                        neighbour_gateway.rank,
                        neighbour_gateway.networkId))
                gateway_network.send_message_to(
                        neighbour_gateway.rank,
                        neighbour_gateway.target,
                        message,
                        False)
        else:
            self.flushprint("destination unknown")

    # A spanning tree request has arrive by the given gateway
    # thread safe
    def handle_spanning_tree(self, infos, gateway, rank_from) :
        decoded_infos = json.loads(infos)
        networkId     = decoded_infos[consts.ID]
        network_rank  = decoded_infos[consts.RANK]
        # creation criteria
        creation      = decoded_infos[consts.CREAT] == consts.TRUE
        # update routing table
        # do not override
        propagate = False
        if creation :
            self.flushprint("TRY create entry for network {} content {}".format(
                networkId, self.routing_table.keys()))
            if networkId not in self.routing_table.keys() :
                # look who is the neighbour, a parent or a child :
                # if it's not in the children list, it's the parent
                # otherwise it's a real crash !
                neighbour = self.erebor_children.get(rank_from+"@"+gateway.ID)
                if neighbour == None :
                    neighbour = self.master_neighbr
                if neighbour == None :
                    self.flushprint("Impossible state")
                else :
                    propagate = True
                    self.flushprint(
                            "create entry for network {} gateway {}@{}".format(
                            networkId,rank_from,gateway.ID))
                    self.routing_table[networkId] = neighbour
        else :
            if networkId in self.routing_table.keys() :
                # here we only remove the key where the netowkId is, because
                # it's a foreigner, so it will not be has a value
                self.flushprint("remove entry for network {} ".format(networkId))
                del self.routing_table[networkId]
                propagate = True
        if propagate :
            self.spanning_propagation([infos])
            if creation :
                # warn the one shot callbacks
                self.call_on_network_up_callbacks(networkId)
            else :
                # warn the one shot callbacks
                self.call_on_network_dn_callbacks(networkId)
        self.print_routing_table()

    def remove_neighbour_from_routing_table(self, neighbour) :
        # When a network die, remove him from the routing table list,
        # remove also the keys where the network is the value
        toremove = []
        for key , val in self.routing_table.items() :
            if val == neighbour :
                toremove.append(key)
        for key in toremove:
            del self.routing_table[key]
        return toremove

    # thread sage
    def print_routing_table(self) :
        for ID, neighbour in self.routing_table.items():
            self.flushprint(" TABLE : to {} gateway {} ".format(ID,
                                                        neighbour.to_string()))

    # Get routing tables from one particular node and incorporate them.
    # Then propagate them if any changes is applyed to my tables
    def incorporate_table(self, list_routes, gateway, from_) :
        self.flushprint("\n received routing table {} from : {}\n".format(
                                                      list_routes, gateway.ID))
        new_gateway = self.routing_table[gateway.ID]
        updates     = False
        for route in list_routes :
            old_gateway = None
            if route in self.routing_table.keys() :
                old_gateway = self.routing_table[route]
            # If the destination is not me, and we need to update it
            if old_gateway != new_gateway and self.networks.get(route) == None :
                updates = True
                self.routing_table[route] = new_gateway
        self.flushprint("routing table {} ".format(self.routing_table.keys()))
        self.print_routing_table()
        # share routing new routing table to all my neighbours except the
        # sender.
        if updates :
            if self.master_network != None and self.master_network.ID != gateway.ID :
                self.send_routing_table_to_father()
            # send my tables to my children, avoiding the sender
            for key, value in self.networks.get_copy().items():
                if self.master_network == None or key != self.master_network.ID :
                    self.send_routing_table_to_all_children(value, [from_+"@"+gateway.ID])


    # Message interpretation ###################################################

    # The interpretation of a trans network message is network dependant.
    def interpret_trans_network(self, message, network, local_from, snode,snetid,
            dnode, dnetid) :
        # the received message is on the JSON form decode it
        try:
            decoded_message = json.loads(message)
        except ValueError :
            return;
        self.flushprint("FROM {} EXECUTE ACTION {}".format(local_from,decoded_message[consts.ACTION]))
        # make it spawn nodes
        if decoded_message[consts.ACTION] == consts.SPAWN :
            callback_object = None
            if int(decoded_message[consts.CALLBACK]) > -1 :
                self.flushprint("callback asked "+network.ID)
                final_cllbck = self.generic_callback
                final_cllbck_value = int(decoded_message[consts.CALLBACK])
                callback_object    = CallbackObject(dnode,
                                                    dnetid,
                                                    snode,
                                                    snetid,
                                                    final_cllbck,
                                                    final_cllbck_value)
            network.spawn_nodes(
                    decoded_message[consts.DEST],
                    decoded_message[consts.DATA],
                    decoded_message[consts.SYNCHRO] == consts.TRUE,
                    False,
                    callback_object)
        # make it display network infos
        elif decoded_message[consts.ACTION] == consts.NETWORK :
            callback_object = None
            if int(decoded_message[consts.CALLBACK]) > -1 :
                self.flushprint("callback asked "+network.ID)
                final_cllbck = self.generic_callback
                final_cllbck_value = int(decoded_message[consts.CALLBACK])
                callback_object    = CallbackObject(dnode,
                                                    dnetid,
                                                    snode,
                                                    snetid,
                                                    final_cllbck,
                                                    final_cllbck_value)
            network.request_network_status(
                    decoded_message[consts.SYNCHRO] == consts.TRUE,
                    True,callback_object)
        # make it update the network
        elif decoded_message[consts.ACTION] == consts.NUPDATE :
            callback_object = None
            if int(decoded_message[consts.CALLBACK]) > -1 :
                self.flushprint("callback asked "+network.ID)
                final_cllbck = self.generic_callback
                final_cllbck_value = int(decoded_message[consts.CALLBACK])
                callback_object    = CallbackObject(dnode,
                                                    dnetid,
                                                    snode,
                                                    snetid,
                                                    final_cllbck,
                                                    final_cllbck_value)
            network.network_update(
                    decoded_message[consts.SYNCHRO] ==
                    consts.TRUE,True,callback_object)
        # make it renumber the network
        elif decoded_message[consts.ACTION] == consts.NNUMBER :
            callback_object = None
            if int(decoded_message[consts.CALLBACK]) > -1 :
                self.flushprint("callback asked "+network.ID)
                final_cllbck = self.generic_callback
                final_cllbck_value = int(decoded_message[consts.CALLBACK])
                callback_object    = CallbackObject(dnode,
                                                    dnetid,
                                                    snode,
                                                    snetid,
                                                    final_cllbck,
                                                    final_cllbck_value)
            network.network_renumber(
                                decoded_message[consts.SYNCHRO] == consts.TRUE,
                                True,callback_object)
        # make it execute a command
        elif decoded_message[consts.ACTION] == consts.EXECUTE :
            # create a new command object, TODO maybe need some refactor
            rc = RunningCommand(network.get_name_of(decoded_message[consts.DEST]),
                                decoded_message[consts.DEST],
                                decoded_message[consts.DATA],
                                self.debug)
            # if the sender asked for to be called when the command is done
            if int(decoded_message[consts.CALLBACK]) > -1 :
                rc.final_cllbck = self.command_done_propagation
                rc.final_cllbck_value = int(decoded_message[consts.CALLBACK])
                rc.snode = snode
                rc.snetid= snetid
                rc.dnode = dnode
                rc.dnetid= dnetid
            else :
                # otherwise, print, for logs
                rc.final_cllbck = rc.print_self
                rc.final_cllbck_value = -1
            # append command on handled running commands
            self.running_commands.append(rc)
            # make the final dest work on the command
            network.execute_on(
                    decoded_message[consts.DEST],
                    decoded_message[consts.DATA],
                    decoded_message[consts.SYNCHRO] == consts.TRUE)
        # make it execute a command
        elif decoded_message[consts.ACTION] == consts.BR_EXEC :
            self.flushprint("yolo")
            bcast = BrodcastRunningCommand(
                    network.spawned_list(),
                    decoded_message[consts.DATA],
                    self.debug,
                    snode,
                    snetid,
                    dnode,
                    dnetid,
                    self.generic_callback,
                    int(decoded_message[consts.CALLBACK]))
            self.running_commands.append(bcast)
            # make the final dest work on the command
            network.broadcast_exec(
                    decoded_message[consts.DEST],
                    decoded_message[consts.DATA],
                    decoded_message[consts.SYNCHRO] == consts.TRUE)
        # make if throw a exec callback
        elif decoded_message[consts.ACTION] == consts.CALLBACK :
            callback_id = decoded_message[consts.CALLBACK]
            data        = decoded_message[consts.DATA]
            callback    = None
            if callback_id in self.execute_callbacks.keys() :
                callback = self.execute_callbacks[callback_id]
            if callback != None :
                callback(data)
        # make it send a message to a particular node
        # do not use this for a trans-message communication
        elif decoded_message[consts.ACTION] == consts.MESSAGE :
            network.send_message_to(
                    decoded_message[consts.DEST],
                    decoded_message[consts.TARGET],
                    decoded_message[consts.DATA],
                    decoded_message[consts.SYNCHRO] == consts.TRUE)
        # send a trans_network message TODO
        elif decoded_message[consts.ACTION] == consts.TNETWRK :
            self.send_trans_network_message_to(
                    decoded_message[consts.DNODE],
                    decoded_message[consts.DNETID],
                    decoded_message[consts.SNODE],
                    decoded_message[consts.SNETID],
                    decoded_message[consts.DATA])
        # send a mpi_trans_network message TODO
        elif decoded_message[consts.ACTION] == consts.MTNETWRK :
            self.send_mpi_trans_network_message_to(
                    decoded_message[consts.DNODE],
                    decoded_message[consts.DNETID],
                    decoded_message[consts.SNODE],
                    decoded_message[consts.SNETID],
                    decoded_message[consts.DATA])
        # make a wait_reduce
        elif decoded_message[consts.ACTION] == consts.WAITR :
            network.wait_reduce(
                    decoded_message[consts.TARGET],
                    decoded_message[consts.SYNCHRO] == consts.TRUE)
        # execute Erebor on
        # start by default a new taktuk network on it
        elif decoded_message[consts.ACTION] == consts.REPLIC :
            self.launch_erebor_on(decoded_message[consts.DEST],
                                  network,
                                  decoded_message[consts.ID],
                                  decoded_message[consts.OPTIONS])
        # start taktuk on specified Erebor
        elif decoded_message[consts.ACTION] == consts.TAKTUK :
            self.start_network(decoded_message[consts.ID],
                               decoded_message[consts.OPTIONS])
        # spanning tree construction
        elif decoded_message[consts.ACTION] == consts.SPANTR :
            try :
                self.flushprint("infos : {}".format(decoded_message[consts.INFOS]))
                for infos in decoded_message[consts.INFOS] :
                    self.flushprint("PROPAGATE !! {}".format(infos))
                    self.handle_spanning_tree(infos, network, local_from)
            except Exception as e:
                self.flushprint(e)
        # terminate children
        elif decoded_message[consts.ACTION] == consts.TERMHIM :
            dest = decoded_message[consts.DEST]
            netw = network.ID
            children = self.erebor_children.get(dest+"@"+netw)
            if children != None :
                self.send_quit_order(network, children)
            else :
                self.flushprint("cannot terminate child")
        elif decoded_message[consts.ACTION] == consts.TERMNET :
            ID = decoded_message[consts.ID]
            network = self.networks.get(ID)
            # for each children under the network, send a quit request
            self.flushprint("TRY TO delete children of network "+ID)
            if network != None :
                to_quit= []
                for children in self.erebor_children.get_copy().values() :
                    if children.networkId == ID :
                        to_quit.append(children)
                self.network_terminaison[ID] = len(to_quit)
                for children in to_quit :
                    self.flushprint("delete children {} of network {}"
                                .format(children.rank, ID))
                    self.send_quit_order(network, children)
                if self.network_terminaison[ID] == 0 :
                    self.purpose_death(network.ID, network)
            else :
                self.flushprint("no network ?")
        # propagate dead nodes
        elif decoded_message[consts.ACTION] == consts.DEAD :
            self.propagate_dead_nodes(snode,
                                      snetid,
                                      decoded_message[consts.DATA].split(","))
        # deploy a MPI message multiplexer per node on the destination network
        elif decoded_message[consts.ACTION] == consts.MPIDEPL :
            on_network = self.networks.get(decoded_message[consts.NETWORK])
            port       = decoded_message[consts.PORT]
            # init the routing table for this group
            self.mpi_routing_table[on_network] = dict()
            # TODO handle the fact that maybe a children is already launched on
            # the node.
            # register the callback and how many replication need to be waited
            spawned = on_network.spawned_list()
            callback_object    = CallbackObject(dnode,
                                                dnetid,
                                                snode,
                                                snetid,
                                                self.generic_callback,
                                                int(decoded_message[consts.CALLBACK]))
            self.mpi_launching_callback[on_network] = callback_object
            self.mpi_launching_to_wait[on_network]  = len(spawned) - 1
            # On every children of the group replicate
            for spawn in spawned :
                if not spawn.me :
                    # Will be called when a replication is done.
                    def after_children_startup(child) :
                        network = self.networks.get(child.networkId)
                        self.start_MPI_bridge_on(child, network.ID, port)
                        self.mpi_launching_to_wait[network] -= 1
                        # when everyone has bootstrap
                        if self.mpi_launching_to_wait[network] == 0 :
                            c = self.mpi_launching_callback[network]
                            # advertise the requester that everything is done
                            c.callback(json.dumps({consts.DATA:"ok"}),
                                        c.dnode,
                                        c.dnetid,
                                        c.snode,
                                        c.snetid,
                                        c.value)
                        # TODO make children spawn MPIHook
                    # register on children startup to make it start its
                    # MPIHook and avertise the sender of the request when
                    # everyone is deployed
                    self.on_children_startup(str(spawn.rank), on_network,
                            after_children_startup)
                    self.launch_erebor_on(str(spawn.rank), on_network)
                else :
                    self.start_mpi_bridhe(on_network.ID, port)
                    # in case of an one element group, still propagate the
                    # correct deployed information
                    if len(spawned) == 1 :
                        c = callback_object
                        c.callback(json.dumps({consts.DATA:"ok"}),
                                   c.dnode,
                                   c.dnetid,
                                   c.snode,
                                   c.snetid,
                                   c.value)
        # each communicator will contact me to populate a binding mpi_rank mpi
        # message multiplexer
        elif decoded_message[consts.ACTION] == consts.MPIREG :
            rank = decoded_message[consts.RANK]
            self.mpi_routing_table[network][rank] = local_from
            if rank in self.mpi_wait_route_msg :
                for message in self.mpi_wait_route_msg[rank] :
                    self.send_mpi_trans_network_message_to(message[0],
                            message[1], message[2], message[3], message[4])
                del self.mpi_wait_route_msg[rank]
        # clean all registered mpi ranks associated with this network.
        elif decoded_message[consts.ACTION] == consts.MPICLN :
            on_network = self.networks.get(decoded_message[consts.NETWORK])
            self.stop_mpi_bridhe(on_network.ID)
            if on_network in self.mpi_routing_table :
                node_list = self.mpi_routing_table[network]
                del self.mpi_routing_table[network]
                # make every node from the node list terminate their MPIHook
                for node in node_list.values() :
                    if node != "0" :
                        children = self.erebor_children.get(node+"@"+on_network.ID)
                        self.stop_MPI_bridge_on(children, on_network.ID)
        elif decoded_message[consts.ACTION] == consts.GETSPWN :
            on_network = self.networks.get(decoded_message[consts.NETWORK])
            slist = on_network.spawned_list()
            returnlist = []
            for spawn in slist :
                if not spawn.me :
                    returnlist.append(str(spawn.rank))
            self.generic_callback(json.dumps({consts.DATA:returnlist}),
                        dnode,
                        dnetid,
                        snode,
                        snetid,
                        int(decoded_message[consts.CALLBACK]))
        elif decoded_message[consts.ACTION] == consts.GETSPWE :
            on_network = self.networks.get(decoded_message[consts.NETWORK])
            slist = on_network.errors_list()
            self.generic_callback(json.dumps({consts.DATA:slist}),
                        dnode,
                        dnetid,
                        snode,
                        snetid,
                        int(decoded_message[consts.CALLBACK]))

    # Registration ############################################################

    def on_dead_nodes(self, callback) :
        self.dead_nodes_clv.append(callback)

    # One shot callback
    # get notify when a direct children shows up
    def on_children_startup(self, rank, network, callback) :
        key = rank+"@"+network.ID
        if key in self.chld_btstrp_clb.keys() :
            self.chld_btstrp_clb[key].append(callback)
        else:
            self.chld_btstrp_clb[key] = [callback]

    # One shot callback
    # Get notified when a network (direct or not) is UP
    def on_network_init(self, networkId, callback) :
        if networkId in self.ntwk_up_cllbks.keys() :
            self.ntwk_up_cllbks[networkId].append(callback)
        else:
            self.ntwk_up_cllbks[networkId] = [callback]

    # One shot callback
    # Get notified when a network (direct or not) is down
    def on_network_shutdown(self, networkId, callback) :
        if networkId in self.ntwk_dn_cllbks.keys() :
            self.ntwk_dn_cllbks[networkId].append(callback)
        else:
            self.ntwk_dn_cllbks[networkId] = [callback]

    # Events ###################################################################

    def dead_bridge_callback(self, network) :
        # TODO
        pass

    def connection_lost_callback(self, snode, snetid, error_nodes, errors_rank) :
        print(snode,snode, snetid, error_nodes, errors_rank)
        pass

    def dead_node_callback(self, snode, snetid, error_nodes, errors_rank) :
        # Advertise running commandes that nodes are deads
        toremove = []
        dispactch= False
        for i in range(0, len(self.running_commands)) :
            running_command = self.running_commands[i]
            ret = running_command.node_deads(error_nodes, errors_rank)
            if ret > -1 :
                dispactch = True
                if ret == 2 :
                    toremove.append(i)
                    self.flushprint("dead command propagation")
                break
        if dispactch :
            for i in toremove :
                del self.running_commands[i]
        # check if an erebor child was running there, and therefore, advertise
        # everyone is death
        for rank in errors_rank :
            self.flushprint("try to find {}".format(rank+"@"+snetid))
            children = self.erebor_children.get(rank+"@"+snetid)
            if children != None :
                children = self.erebor_children.remove(rank+"@"+snetid)
                self.flushprint("children dead !! {}".format(children.to_string()))
                self.children_is_dead(children, snetid)
            else :
                self.flushprint("not found {}".format(rank+"@"+snetid))
        # Propagate information to the root Erebor
        self.propagate_dead_nodes(snode, snetid, error_nodes)

    def subprocess_callback(self, networkId, txt) :
        self.flushprint("SUBPROCESS {}".format(txt))
        dispactch = False
        toremove = []
        for i in range(0, len(self.running_commands)) :
            running_command = self.running_commands[i]
            if networkId == running_command.dnetid :
                ret = running_command.test_and_dispatch(txt)
                if ret > -1 :
                    dispactch = True
                    if ret == 2 :
                        toremove.append(i)
                        self.flushprint("end propagation")
                    break
        if dispactch == False :
            self.flushprint(bcolors.WARNING+ txt +bcolors.ENDC)
        else :
            for i in toremove :
                del self.running_commands[i]


    # Is called when a trans network message arrive here
    # Need to chose if the message has to be treated here or forwarded
    def trans_network_callback(self, message, rank_from) :
        source      = message[consts.FROM]
        destination = message[consts.DEST]
        self.flushprint(
                "a trans network message arrived "+source+" -> "+destination)
        package     = message[consts.DATA]
        # source and destination are on the form number@networkId
        snode       = source.split("@")[0]
        snetwork    = source.split("@")[1]
        dnode       = destination.split("@")[0]
        dnetwork    = destination.split("@")[1]
        # look if I am the right destination Erebor.
        # -> destination need to match on of my network identifier
        network     = self.networks.get(dnetwork)
        # If I have a match, it means the package belong to one of my networks
        if network != None :
            self.flushprint("local network routing {}".format(dnode))
            # check my rank to see is i'm equivalent to dnode
            if network.state == consts.RUNING :
                dnode_list = self.helper.build_list("["+dnode.replace("/",",")+"]")
                self.flushprint(" node list {}".format(dnode_list))
                # If I am the destination
                if network.rank in dnode_list :
                    self.interpret_trans_network(package, network,
                            rank_from,snode,snetwork, dnode, dnetwork)
                else :
                    self.flushprint("not destination, forwarding")
                    self.send_trans_network_message_to(
                            dnode,
                            dnetwork,
                            snode,
                            snetwork,
                            package)
    # Is called when a trans network message arrive here
    # Need to chose if the message has to be treated here or forwarded
    def mpi_trans_network_callback(self, message, rank_from) :
        source      = message[consts.FROM]
        destination = message[consts.DEST]
        m           = message[consts.DATA]
        self.flushprint(
                "a mpi trans network message arrived "+source+" -> "+destination)
        package     = message[consts.DATA]
        # source and destination are on the form number@networkId
        mpi_snode   = source.split("@")[0]
        snetwork    = source.split("@")[1]
        mpi_dnode   = destination.split("@")[0]
        dnetwork    = destination.split("@")[1]
        # look if I am the right destination Erebor.
        # -> destination need to match on of my network identifier
        network     = self.networks.get(dnetwork)
        # If I have a match, it means the package belong to one of my networks
        if network != None :
            self.flushprint("local network routing {}".format(mpi_dnode))
            # check my rank to see is i'm equivalent to mpi_dnode
            if network.state == consts.RUNING :
                mpi_hook = self.mpi_bridges[network]
                # If I am the destination
                if mpi_hook.is_destination_for(mpi_dnode) :
                    mpi_hook.send_message_to_local_mpi_rank(mpi_dnode,mpi_snode,
                                                            snetwork, m)
                else :
                    self.send_mpi_trans_network_message_to(
                                    mpi_dnode,
                                    dnetwork,
                                    mpi_snode,
                                    snetwork,
                                    package)
            else :
                # TODO if network.state != RUNNING then 
                # buffer the message for later
                pass 
        else :
            self.flushprint(" forward for {}@{}, received by {}@{}".format(
                    mpi_dnode, dnetwork, mpi_snode, snetwork))
            # If not, forward message
            self.send_mpi_trans_network_message_to(
                            mpi_dnode,
                            dnetwork,
                            mpi_snode,
                            snetwork,
                            package)

    # Is called when a trans network message arrive here
    # Need to chose if the message has to be treated here or forwarded
    def trans_network_callback(self, message, rank_from) :
        source      = message[consts.FROM]
        destination = message[consts.DEST]
        self.flushprint(
                "a trans network message arrived "+source+" -> "+destination)
        package     = message[consts.DATA]
        # source and destination are on the form number@networkId
        snode       = source.split("@")[0]
        snetwork    = source.split("@")[1]
        dnode       = destination.split("@")[0]
        dnetwork    = destination.split("@")[1]
        # look if I am the right destination Erebor.
        # -> destination need to match on of my network identifier
        network     = self.networks.get(dnetwork)
        # If I have a match, it means the package belong to one of my networks
        if network != None :
            self.flushprint("local network routing {}".format(dnode))
            # check my rank to see is i'm equivalent to dnode
            if network.state == consts.RUNING :
                dnode_list = self.helper.build_list("["+dnode.replace("/",",")+"]")
                self.flushprint(" node list {}".format(dnode_list))
                # If I am the destination
                if network.rank in dnode_list :
                    self.interpret_trans_network(package, network,
                            rank_from,snode,snetwork, dnode, dnetwork)
                else :
                    self.flushprint("not destination, forwarding")
                    self.send_trans_network_message_to(
                            dnode,
                            dnetwork,
                            snode,
                            snetwork,
                            package)

            else :
                # TODO if network.state != RUNNING then 
                # buffer the message for later
                pass 
        else :
            self.flushprint(" forward for {}@{}, received by {}@{}".format(
                    dnode, dnetwork, snode, snetwork))
            # If not, forward message
            self.send_trans_network_message_to(
                            dnode,
                            dnetwork,
                            snode,
                            snetwork,
                            package)

    # Is called when a the new network object is ready
    # propagate the information to the root
    def new_network_callback(self, infos) :
        decoded_infos = json.loads(infos)
        networkId     = decoded_infos[consts.ID]
        network_rank  = decoded_infos[consts.RANK]
        self.flushprint("network started "+networkId)
        # warn the one shot callbacks
        self.call_on_network_up_callbacks(networkId)
        # add the creation criteria
        decoded_infos[consts.CREAT] = consts.TRUE
        # update spanning tree
        infos = json.dumps(decoded_infos)
        # propagate spanning tree
        self.spanning_propagation([infos])

    # Is called when a network object is down
    # propagate the information
    def del_network_callback(self, network_rank, networkId):
        self.flushprint("network down "+networkId)
        data  = json.dumps({
            consts.FROM:network_rank,
            consts.TYPE:consts.INFOS,
            consts.RANK:network_rank,
            consts.ID:networkId,
            consts.CREAT:consts.FALSE,
            });
        # warn the one shot callbacks
        self.call_on_network_dn_callbacks(networkId)
        self.spanning_propagation([data])

    # Is called by the network when a child shows up
    def child_connect_callback(self, rank, networkId) :
        self.flushprint("a child has come "+rank+" "+networkId)
        child = Neighbour(rank, networkId)
        self.erebor_children.add(rank+"@"+networkId, child)
        # notify the registered that a child has come
        network = self.networks.get(networkId)
        if network != None :
            self.call_on_children_startup_callbacks(rank, network, child)

    # Is called by the network when a order shows up
    def order_callback(self, order, rank_from, networkId) :
        self.flushprint("a order has come : "+str(order[consts.ORDER]))
        # If parent ask me to quit
        if order[consts.ORDER] == consts.QUIT :
            self.terminate()# launch terminaison sequence
        # start a new
        elif order[consts.ORDER] == consts.TAKTUK :
            self.start_network(order[consts.ID],
                               order[consts.OPTIONS])
        # If children notifies me he quits
        elif order[consts.ORDER] == consts.ACKQUIT :
            # do not trigger any callback on terminaison if we are aiming to
            # kill ourself.
            if self.state == consts.TERMINAISON_PENDING :
                self.to_wait_terminaison -= 1
                if self.to_wait_terminaison == 0 :
                    #self.kill_them()
                    self.kill_me()
                else :
                    self.flushprint("wait for {} children to terminate"
                            .format(self.to_wait_terminaison))
            else :
                # a children has died need to update routing table
                # first, pick the neighbour and neighbour from children list
                neighbour = self.erebor_children.remove(rank_from+"@"+networkId)
                # kill child
                network = self.networks.get(networkId)
                self.kill_it(network,neighbour)
                self.children_is_dead(neighbour, networkId)
        # If i'm asked to start my MPI bridge
        elif order[consts.ORDER] == consts.STRTMPI :
            networkId = order[consts.NETWORK]
            port      = order[consts.PORT]
            self.start_mpi_bridhe(networkId, port)
        # If i'm asked to stop the MPI bridge
        elif order[consts.ORDER] == consts.STOPMPI :
            networkId = order[consts.NETWORK]
            self.stop_mpi_bridhe(networkId)

    def children_is_dead(self, neighbour, networkId) :
        self.flushprint("children is dead")
        # propagate.
        # determine who where the children's networks
        # and remove them
        childrens_networks = self.remove_neighbour_from_routing_table(neighbour)
        # then propagate the terminaison information on the deleted
        # networks
        for net_id in childrens_networks :
            self.del_network_callback("0", net_id)
        # are we trying to shutdown a network ?
        if networkId in self.network_terminaison.keys() :
            self.network_terminaison[networkId] -= 1
            if self.network_terminaison[networkId] == 0 :
                network = self.networks.get(networkId)
                self.purpose_death(networkId, network)

    def purpose_death(self, networkId, network) :
        self.flushprint("terminate network {} on purpose".format(networkId))
        # terminate the network
        network.terminate()
        # propagate the information that the network ID is down
        self.del_network_callback("0", networkId)
        self.networks.remove(networkId)

    # Propagation ##############################################################

    def generic_callback(self, data, dnode, dnetid, snode, snetid, callback_value):
        message = json.dumps({
            consts.ACTION:consts.CALLBACK,
            consts.CALLBACK:callback_value,
            consts.DATA:data
            })
        # inverse source and destination to reply
        self.send_trans_network_message_to(snode,
                                                  snetid,
                                                  dnode,
                                                  dnetid,
                                                  message)

    # called by the command when a callback on the network is needed
    def command_done_propagation(self, stdout, stderr, status, snode, snetid,
                                                dnode, dnetid, callback_value):
        final = json.dumps({
            consts.STDOUT:stdout,
            consts.STDERR:stderr,
            consts.STATUS:status
            })
        message = json.dumps({
            consts.ACTION:consts.CALLBACK,
            consts.CALLBACK:callback_value,
            consts.DATA:final
            })
        # inverse source and destination to reply
        self.send_trans_network_message_to(snode,
                                                  snetid,
                                                  dnode,
                                                  dnetid,
                                                  message)

    def propagate_dead_nodes(self, snode, snetid, error_nodes):
        if (self.master_network != None) :
            message = json.dumps({
                consts.ACTION:consts.DEAD,
                consts.CALLBACK:callback_value,
                consts.DATA:",".join(error_nodes)
                })
            self.flushprint( "DEAD PROPAGATE {} to master network".format(infos))
            self.send_trans_network_message_to(
                            "0",
                            self.master_network.ID,
                            snode,
                            snetid,
                            message)
        else :
            self.call_on_dead_nodes_callbacks(snode, snetid, error_nodes)

    # send the information of a new network on parent
    # put elements to avoid warning on the avoid list
    def spanning_propagation(self, infos):
        self.flushprint("SPANNING enter PROPAGATE")
        # warn direct parents and children
        # with a spanning tree call
        # the message is from the new network to the other one
        message = json.dumps({
                consts.ACTION:consts.SPANTR,
                consts.INFOS:infos})
        if self.master_network != None :
            self.flushprint(
                    "SPANNING PROPAGATE {} to master network".format(infos))
            self.send_trans_network_message_to(
                        "0",
                        self.master_network.ID,
                        self.master_network.rank,
                        self.master_network.ID,
                        message)

    # Callbacks ################################################################

    # propagate the child startup event on registered callbacks
    def call_on_dead_nodes_callbacks(self, source_rank, networkId, error_nodes) :
        for callback in self.dead_nodes_clv :
            callback(source_rank, networkId, error_nodes)

    # propagate the child startup event on registered callbacks
    def call_on_children_startup_callbacks(self, rank, network, child) :
        key = rank+"@"+network.ID
        if key in self.chld_btstrp_clb.keys() :
            callbacks = self.chld_btstrp_clb[key]
            for callback in callbacks :
                callback(child)
            self.chld_btstrp_clb[key] = []

    # propagate the network startup event on registered callbacks
    def call_on_network_up_callbacks(self, networkId) :
        if networkId in self.ntwk_up_cllbks.keys() :
            callbacks = self.ntwk_up_cllbks[networkId]
            for callback in callbacks :
                callback(networkId)
            self.ntwk_up_cllbks[networkId] = []

    # propagate the network down event on registered callbacks
    def call_on_network_dn_callbacks(self, networkId) :
        if networkId in self.ntwk_dn_cllbks.keys() :
            callbacks = self.ntwk_dn_cllbks[networkId]
            for callback in callbacks :
                callback(networkId)
            self.ntwk_dn_cllbks[networkId] = []

    # Bootstrap ################################################################

    # Depending on the root state of this instance, it will connect to the
    # master node or spawn a taktuk network.
    # if this instance is root, the networkId is the networkId of its
    # network otherwise, the networkId is the one of its master network
    def bootstrap(self, networkId, options="", log_time=None):
        if log_time != None :
            self.flog_time = log_time
        if self.isRoot :
            # This case is the case for the root node, only this one will be
            # master, its network must be public.
            self.start_network(networkId, options)
        else :
            self.connect_to_master_network(networkId)
        # update state
        self.state = consts.RUNING

    # Connect to an existing network, for whom I am a slave
    # There can only be one of those
    # thread safe
    def connect_to_master_network(self, networkId):
        self.master_network = Network(
                self.add_to_queue,
                False,
                networkId,
                self.debug_list,
                self.taktuk_path)
        self.networks.add(networkId, self.master_network)
        # register callbacks to the network
        self.register_callback_on_network(self.master_network)
        # for routing purposes
        self.master_neighbr = Neighbour("0", networkId)
        self.routing_table[networkId] = self.master_neighbr
        # start network
        self.master_network.go()

    # Start a new network, for whom I am the master
    def start_network(self, networkId, options=""):
        network = Network(self.add_to_queue, True, networkId, self.debug_list,
                          self.taktuk_path, options, self.flog_time)
        self.networks.add(networkId, network)
        # register callbacks to the network
        self.register_callback_on_network(network)
        # holder for routing
        # this way in the routing table, the route for a network that I master
        # will always be me.
        self.routing_table[networkId] = Neighbour("0", networkId)
        # start network
        network.go()

    # Registers a bunch of callbacks on the network, need to be called before
    # the network startup
    def register_callback_on_network(self, network):
        network.register_on_subprocess_callback(self.subprocess_callback)
        # Get notified by the network when a new order shows up
        network.register_order_callback(self.order_callback)
        # Get notified by the network when a new child shows up
        network.register_children_callback(self.child_connect_callback)
        # Get notified when the network bootstrap
        network.register_network_bootstrap_callback(self.new_network_callback)
        # Get notified when a trans network message arrive
        network.register_mpi_trans_network_callback(self.mpi_trans_network_callback)
        # Get notified when a trans network message arrive
        network.register_trans_network_callback(self.trans_network_callback)
        # Get notified when a node dies
        network.register_on_dead_nodes(self.dead_node_callback)
        # Get notified when a connection is lost
        network.register_on_lost_nodes(self.connection_lost_callback)
        # Get notified when a bridge dies
        network.register_on_dead_bridge(self.dead_bridge_callback)

    # Start a new network, for whom I am the master
    def start_mpi_bridhe(self, networkId, port) :
        network = self.networks.get(networkId)
        if network != None :
            self.mpi_bridges[network] = MPIHook(self.add_to_queue,
                                                self.send_trans_network_message_to,
                                                self.send_mpi_trans_network_message_to,
                                                port,
                                                network,
                                                self.debug_list)
            self.mpi_bridges[network].start()

    # Start a new network, for whom I am the master
    def stop_mpi_bridhe(self, networkId) :
        network = self.networks.get(networkId)
        if network != None :
            self.mpi_bridges[network].kill()

    def process_messages(self) :
        while self.run :
            try :
                event = self.message_queue.get(True, 1)
            except queue.Empty:
                pass
            else :
                #try :
                event.execute()
                #except Exception as e :
                #    self.flushprint("{}".format(e))

    def add_to_queue(self, event) :
        self.message_queue.put(event)

    # Replication #############################################################

    # TODO path problem
    # The given ID to the children is ours. This will label the network between
    # us with its group ID. Remember IDs are attached to networks.
    def launch_erebor_on(self, dest, network, networkId=None, options=""):
        # If needed, start, when the child is ready a new network on it
        if networkId != None :
            def after_chilren_startup(child) :
                #self.log_time("erebor children appear")
                self.start_taktuk_on(child, networkId, options)
            self.on_children_startup(dest, network, after_chilren_startup)
        debug = ""
        if len(self.debug_list) > 0 :
            debug = "--debug="+(",".join(self.debug_list))
        # launch the children
        name    = pwd.getpwuid(os.getuid()).pw_name
        command = ""
        epath   = ""
        tpath   = ""
        if not self.erebor_path == "" :
            epath = " --erebor-path={} ".format(self.erebor_path)
            command = "{}erebor/erebor_launch.sh {}".format(self.erebor_path, self.erebor_path)
        else :
            command = "main.py"
        if not self.taktuk_path == "" :
            tpath = " --taktuk-path={} ".format(self.taktuk_path)
        network.execute_on(dest,
                command+" --non-root --id="+network.ID+" "+debug+epath+tpath,
                False, False)
        #self.log_time("order to taktuk")

    # Ask the Erebor children to startup Taktuk.
    # -> only work with direct children
    # It implies the creation of a new network on the child with the given
    # networkID
    def start_taktuk_on(self, children, networkId, options="") :
        network = self.networks.get(children.networkId)
        if network != None :
            order = json.dumps({
                    consts.TYPE:consts.ORDER,
                    consts.ORDER:consts.TAKTUK,
                    consts.ID:networkId,
                    consts.OPTIONS:options
                    });
            network.send_message_to(children.rank, children.target, order, False)

    # Ask the Erebor children to start a MPIBridge on the given networkId
    # -> only work with direct children
    def start_MPI_bridge_on(self, children, networkId, port) :
        network = self.networks.get(children.networkId)
        if network != None :
            order = json.dumps({
                    consts.TYPE:consts.ORDER,
                    consts.ORDER:consts.STRTMPI,
                    consts.NETWORK:networkId,
                    consts.PORT:port
                    });
            network.send_message_to(children.rank, children.target, order, False)

    # Ask the Erebor children to start a MPIBridge on the given networkId
    # -> only work with direct children
    def stop_MPI_bridge_on(self, children, networkId) :
        network = self.networks.get(children.networkId)
        if network != None :
            order = json.dumps({
                    consts.TYPE:consts.ORDER,
                    consts.ORDER:consts.STOPMPI,
                    consts.NETWORK:networkId
                    });
            network.send_message_to(children.rank, children.target, order, False)

    # Terminaison #############################################################

    def terminate(self):
        #print_threading()
        if self.state != consts.TERMINAISON_PENDING :
            #self.flushprint(" j'me casse !")
            self.state = consts.TERMINAISON_PENDING
            # terminate all children and wait for their completion
            self.to_wait_terminaison = self.erebor_children.length()
            # If there is children to wait, kill them before
            self.flushprint(" to wait {}".format(self.to_wait_terminaison))
            if self.to_wait_terminaison > 0 :
                self.terminate_children()
            else :
                self.kill_me()

    def terminate_children(self):
        self.flushprint("terminate children")
        for children in self.erebor_children.get_copy().values() :
            # send order to terminate to children
            network = self.networks.get(children.networkId)
            self.send_quit_order(network, children)

    def send_quit_order(self, network, children):
        network.send_message_to(children.rank, "all", consts.QUIT_ORDER, True)

    def kill_them(self):
        for child in self.erebor_children.get_copy().values() :
            network = self.networks.get(child.networkId)
            self.kill_it(network, child)

    def kill_it(self, network, child):
        network.kill_all(child.rank)

    def kill_me(self):
        self.flushprint("kill me")
        self.terminate_non_master_networks()
        # if there is a father, advertise I'm done
        if self.master_network != None :
            self.flushprint("send ackquit")
            self.master_network.send_message_to(
                "0",
                "all",
                consts.ACK_QUIT_ORDER, True)
            # terminate master network
            self.master_network.terminate()
        else :
            self.flushprint("no father to advertise")
        # kill myself
        self.end_lock.release()
        self.flushprint("ready to die")
        self.run = False

    # is called when all children are down
    def terminate_non_master_networks(self):
        self.flushprint("terminate non master networks")
        # terminate the networks
        for key, value in self.networks.get_copy().items():
            if self.master_network == None or key != self.master_network.ID :
                self.flushprint("terminate network "+value.ID)
                value.terminate()
