#!/bin/bash
base="$1"
shift
export PATH=$PATH:$base:$base/isengard:$base/erebor:$base/task_lib
echo $PATH
export PYTHONPATH=$PYTHONPATH:$base:$base/isengard:$base/erebor:$base/task_lib
echo $PYTHONPATH

main.py $@
