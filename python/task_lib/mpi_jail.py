import json
from isengard       import consts
from numbered_group import NumberedGroup

class MPIJail(NumberedGroup):

    server_number = 6000

    def __init__(self, name, ID, node_list, base_rank, base_network, timeout=-1):
        NumberedGroup.__init__(self, name, ID, node_list, base_rank,
                               base_network, timeout)
        MPIJail.server_number+=1
        self.server_number = MPIJail.server_number
        self.server_socket = "{}".format(self.server_number)

    def start_numbered_group(self) :
        def mpi_session_started(data) :
            self.start_mpi_jail_group()
        self.framework.start_mpi_session(self.ID,
                self.root_r,
                self.root_n,
                self.server_socket,
                mpi_session_started)

    # to override
    def start_mpi_jail_group(self) :
        self._run(self.name)
