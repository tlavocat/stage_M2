#!/usr/bin/env python3
import getopt
import time
import json
import sys
import signal
import threading
from erebor   import main
from erebor   import FrameworkControler
from erebor   import Erebor
from isengard import consts

class TaskProcessor(FrameworkControler):
    def __init__(self, erebor, ID, node_list, tfile=None):
        FrameworkControler.__init__(self, erebor, ID, tfile)
        self.groups = dict()
        self.tasks  = []
        self.register_tasks()
        for task in self.tasks :
            task.set_framework(self)
            self.groups[task.name] = task
            task.register_done_callback(self.task_done)
            task.register_sick_callback(self.task_sick)
            task.register_run_callback(self.task_running)
            task.register_err_callback(self.task_error)
            task.register_timeout_callback(self.task_timeout)
            task.register_cance_callback(self.task_cancel)
        self.to_wait = len(self.tasks)

    # to override
    def register_tasks(self) :
        pass

    def end(self) :
        self.to_wait -=1
        if self.to_wait == 0 :
            self.erebor.terminate()
        elif self.to_wait < 0 :
            print(consts.error("ouch"))

    def task_running(self, name) :
        print(consts.blue("{} is {}".format(name, self.groups[name].state)))

    def task_error(self, name) :
        print(consts.green("{} is {}".format(name, self.groups[name].state)))
        self.end()

    def task_timeout(self, name) :
        print(consts.green("{} is {}".format(name, self.groups[name].state)))
        self.end()

    def task_sick(self, name) :
        print(consts.green("{} is {}".format(name, self.groups[name].state)))
        self.end()

    def task_done(self, name) :
        print(consts.green("{} is {}".format(name, self.groups[name].state)))
        self.end()

    def task_cancel(self, name) :
        print(consts.green("{} is {}".format(name, self.groups[name].state)))
        self.end()

    # Run some tests
    def start(self, networkId):
        self.networkId = networkId
        print("processor start")
        for task in self.tasks :
            task.init_task()

    # Add a task on the fly
    def add_task(self, task) :
        self.to_wait += 1
        self.tasks.append(task)
        self._init_task(task)

if __name__ == "__main__":
    sys.exit(main(sys.argv, Processor))
