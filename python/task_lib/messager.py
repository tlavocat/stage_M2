import re
import json
from isengard import consts
from task     import Task
from mpi_jail import MPIJail

class Barrier(Task):

    def __init__(self, name, pattern, group_to_end, local_ID, how, timeout=-1) :
        Task.__init__(self, name, timeout)
        self.group_to_end  = group_to_end
        self.how           = how
        self.local_ID      = local_ID
        self.message_regex = re.compile(pattern)

    def terminate_task(self) :
        self.group_to_end.terminate_task()
        Task.terminate_task(self)

    def run(self) :
        self.network = self.framework.get_network(self.local_ID)
        self.network.register_on_bridge_generic_messages(self.receive_gmsg)

    def receive_gmsg(self, data) :
        decoded_message = json.loads(data)
        decoded_data    = json.loads(decoded_message[consts.DATA])
        if self.message_regex.match(decoded_data[consts.DATA]) != None :
            print(data)
            self.how = self.how -1
            if self.how == 0 :
                self.terminate_task()
