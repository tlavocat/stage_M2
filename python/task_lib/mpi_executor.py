import json
from isengard import consts
from executor import Executor
from mpi_jail import MPIJail

class MPIExecutor(Executor):

    def __init__(self, name, command, jail, end=False, timeout=-1) :
        Executor.__init__(self, jail, end, name, timeout)
        self.command = "{} -N {} -P {}".format(command, self.group.ID,
                                          self.group.server_number)

    def run(self) :
        self.framework.exec_on("0",
                self.command,
                consts.TRUE,
                "0",
                self.group.ID,
                self.group.root_r,
                self.group.root_n,
                self.command_done)

    # to override
    def command_done(self, data) :
        self.print_data(data, self.group.ID)
        #self.framework.end_mpi_session(self.group.ID,
        #    self.group.root_r,
        #    self.group.root_n)
        Executor.terminate_task(self)
