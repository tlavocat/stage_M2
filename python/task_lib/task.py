from threading import Timer
from isengard  import consts
from isengard  import Event

class LaunchDependency :

    def __init__(self, name) :
        self.dependency = set()
        self.tasks = dict()
        self.name = name

    def add_dependency(self, task, state) :
        self.tasks[task.name] = task
        #print("{} add dependency on {} on state {}".format(self.name, task.name, state))
        self.dependency.add(task.name+"@"+state)

    def check_depency(self, task_name) :
        if task_name in self.tasks :
            task   = self.tasks[task_name]
            current= task.state
            if task_name+"@"+current in self.dependency :
                #print("{} found {}".format(self.name, task_name+"@"+current))
                self.dependency.remove(task.name+"@"+current)
                #print("{} rest : {}".format(self.name, len(self.dependency)))
        return len(self.dependency) == 0


class Task:

    def __init__(self, name, timeout=-1, root_r="0", root_n="root"):
        self.name          = name            # Way to identify the node
        self.root_r        = root_r          # Node where root erebor is running
        self.root_n        = root_n          # Name of the root erebor instance
        self.state         = consts.BOOSTRAP # init state
        self.sick_callback = []
        self.errr_callback = []
        self.done_callback = []
        self.run_callback  = []
        self.timo_callback = []
        self.cncl_callback = []
        self.timer         = None
        if timeout > -1 :
            #print("timer launcher")
            self.timer = Timer(timeout,self.timeout_task)
        self.run_dependency    = list()
        self.cancel_dependency = list()

    def set_framework(self, framework):
        self.framework = framework

    # callback registration

    def register_sick_callback(self, callback) :
        self.sick_callback.append(callback);
        return self

    def register_done_callback(self, callback) :
        self.done_callback.append(callback);
        return self

    def register_run_callback(self, callback) :
        self.run_callback.append(callback);
        return self

    def register_err_callback(self, callback) :
        self.errr_callback.append(callback);
        return self

    def register_timeout_callback(self, callback) :
        self.timo_callback.append(callback);
        return self

    def register_cance_callback(self, callback) :
        self.cncl_callback.append(callback);
        return self

    # initialization and running

    # to override
    def initialize(self) :
        pass

    # delayed run

    def add_dependency_conjunction(self, dependencies, run) :
        if run :
            fun = self._run_dependency_checker
            dep = LaunchDependency(self.name)
            self.run_dependency.append(dep)
        else :
            fun = self._cancel_dependency_checker
            dep = LaunchDependency(self.name)
            self.cancel_dependency.append(dep)
        for task,state in dependencies.items() :
            assert state in [consts.RUNING, consts.ERROR, consts.SICK,
                             consts.DONE, consts.CANCELED, consts.TIMEOUT]
            dep.add_dependency(task, state)
            if state == consts.RUNING :
                task.register_run_callback(fun)
            elif state == consts.ERROR :
                task.register_err_callback(fun)
            elif state == consts.SICK :
                task.register_sick_callback(fun)
            elif state == consts.DONE :
                task.register_done_callback(fun)
            elif state == consts.TIMEOUT :
                task.register_timeout_callback(fun)
            elif state == consts.CANCELED :
                task.register_cance_callback(fun)
        return self

    def _run_dependency_checker(self, name) :
        run_dependency = False
        for dep in self.run_dependency :
            if dep.check_depency(name) :
                run_dependency = True
                #print(self.name+" valid")
                break
            else :
                pass
                #print(self.name+"not yet valid")
        if run_dependency :
            self.run_dependency = []
            self._run()

    def _cancel_dependency_checker(self, name) :
        run_dependency = False
        for dep in self.cancel_dependency :
            if dep.check_depency(name) :
                run_dependency = True
                break
        if run_dependency :
            self.cancel_dependency = []
            self._cancel()

    # to override
    def init_task(self) :
        if self.state == consts.BOOSTRAP :
            #print('{} init'.format(self.name))
            self.state = consts.INIT
            self.initialize()

    # run
    def _run(self, name=""):
        #print('{} try _run {}'.format(self.name, self.state))
        if self.state == consts.INIT :
            self.run()
            if self.timer != None :
                self.timer.start()
            self.notify_run()
        else :
            #print('{} cant _run'.format(self.name))
            pass

    def _timeout(self, name="") :
        self.timeout()

    def _cancel(self, name="") :
        if self.state == consts.INIT or self.state == consts.RUNING :
            self.cancel()

    def _error(self, name="") :
        self.error()

    # in case of timeout
    def timeout_task(self) :
        self.framework.erebor.add_to_queue(Event(self._timeout, self.name))

    # to override
    def run(self) :
        #print("run "+self.name+" "+str(type(self)))
        self.notify_run()

    # to override
    def terminate_task(self) :
        if self.timer != None :
            self.timer.cancel()
        if self.state == consts.RUNING :
            #print("{} terminate done ".format(self.name))
            self.notify_done()
        else :
            pass
            #print("{} terminate not done  ".format(self.name))

    # to override
    def timeout(self) :
        self.notify_timeout(self.name)
        self.terminate_task()

    # to override
    def cancel(self) :
        self.notify_cancelation(self.name)
        self.terminate_task()

    # to override
    def error(self) :
        self.notify_error(self.name)
        self.terminate_task()

    # event propagation -------------------------------------------------------
    def notify_sick(self, a=None) :
        if self.state != consts.SICK :
            self.state = consts.SICK
            for callback in self.sick_callback :
                callback(self.name)

    def notify_run(self, a=None) :
        if self.state != consts.RUNING :
            self.state = consts.RUNING
            for callback in self.run_callback :
                callback(self.name)

    def notify_done(self, a=None) :
        if self.state != consts.DONE :
            self.state = consts.DONE
            for callback in self.done_callback :
                callback(self.name)

    def notify_error(self, a=None) :
        if self.state != consts.ERROR :
            self.state = consts.ERROR
            for callback in self.errr_callback :
                callback(self.name)

    def notify_timeout(self, a=None) :
        if self.state != consts.TIMEOUT :
            self.state = consts.TIMEOUT
            for callback in self.timo_callback :
                callback(self.name)

    def notify_cancelation(self, a=None) :
        if self.state != consts.CANCELED :
            self.state = consts.CANCELED
            for callback in self.cncl_callback :
                callback(self.name)
