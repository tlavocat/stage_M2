import json
from isengard import consts
from group    import Group
from executor import Executor

class Broadcaster(Executor):

    def __init__(self, name, command, group, end=False, timeout=-1,
            fail_threshold=0) :
        Executor.__init__(self, group, end, name, timeout)
        self.command = command
        self.fail_threshold = fail_threshold

    def run(self) :
        self.framework.broadcast_exec_on("0",
                               self.command,
                               consts.TRUE,
                               "0",
                               self.group.ID,
                               self.group.root_r,
                               self.group.root_n,
                               self.command_done)
    # to override
    def command_done(self, data) :
        decoded_data = json.loads(data)
        stdouts = decoded_data[consts.STDOUT]
        stderrs = decoded_data[consts.STDERR]
        statuss = decoded_data[consts.STATUS]
        nbfail  = 0
        for key, value in statuss.items() :
            status = statuss[key]
            stdout = []
            stderr = []
            if key in stdouts :
                stdout = stdouts[key]
            if key in stderrs :
                stderr = stderrs[key]
            self.printt_decoded_data(stderr, stdout, status[0], key)
            exstat = status[0].split("Exited with status")
            stat   = int(exstat[len(exstat)-1])
            if stat != 0 :
                nbfail += 1
        if nbfail <= self.fail_threshold :
            Executor.terminate_task(self)
        else :
            self.notify_error()
