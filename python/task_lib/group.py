import json
from task     import Task
from isengard import consts

# A group can only be attached to its current Erebor instance.
# For now, only properly working on root group.
class Group(Task):

    def __init__(self, name, ID, node_list, base_rank, base_network, timeout=-1,
            max_boot_error=0):
        Task.__init__(self, name, timeout)
        self.name          = name       # Way to identify the node
        self.base_rank     = base_rank  # node rank on parent network
        self.base_network  = base_network # parent network
        self.base          = "0"        # A group is always attached to the 0
        self.ID            = ID         # ID of the group
        self.node_list     = node_list  # nodes composing the group
        self.state         = consts.BOOSTRAP # init state
        self.max_boot_error= max_boot_error

    def dead_nodes(self, rank, ID, nodes) :
        if ID == self.ID :
            if self.state == consts.RUNING :
                self.notify_sick()

    def initialize(self) :
        self.framework.erebor.on_dead_nodes(self.dead_nodes)
        # called when the new group named ID is started on base
        def group_up(networkId):
            # called when all nodes are connected on base
            if self.node_list != "" :
                def nodes_spawned(error_nodes):
                    def got_spawn_errors(data) :
                        decoded_data = json.loads(data)
                        error_list   = decoded_data[consts.DATA]
                        if len(error_list) <= self.max_boot_error :
                            self.start_group()
                        else :
                            self.notify_sick()
                    self.framework.acquire_spawn_errors(self.ID,self.root_r,
                                                      self.root_n,got_spawn_errors)
                # make 0 on ID group spawn nodes
                self.framework.spawn_on(
                              "0",
                              self.node_list,
                              consts.FALSE,
                              "0",
                              self.ID,
                              self.root_r,
                              self.root_n,
                              nodes_spawned)
            else :
                self.start_group()
        # register on network ID init
        self.framework.erebor.on_network_init(self.ID, group_up)
        # make base spawn a new network named ID
        self.framework.new_group_on(self.ID, "", self.base_rank,
                                    self.base_network, self.root_r, self.root_r);

    # to override
    def start_group(self) :
        self._run(self.name)

    def terminate_task(self) :
        self.framework.erebor.on_network_shutdown(self.ID, self.notify_done)
        self.framework.delete_network(self.ID, "0", self.ID, self.root_r,
                                      self.root_n)
        Task.terminate_task(self)
