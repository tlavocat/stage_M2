from isengard import consts
from group    import Group

class NumberedGroup(Group):

    def __init__(self, name, ID, node_list, base_rank, base_network, timeout=-1):
        Group.__init__(self, name, ID, node_list, base_rank, base_network,
                       timeout)

    def start_group(self) :
        if self.node_list != "" :
            def numbered(data) :
                self.start_numbered_group()
            self.framework.network_update_on(
                              consts.TRUE,
                              "0",
                              self.ID,
                              self.root_r,
                              self.root_n,
                              numbered)
        else :
            self.start_numbered_group()

    # to override
    def start_numbered_group(self) :
        self._run(self.name)
