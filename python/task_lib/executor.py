import json
from task     import Task
from group    import Group
from isengard import consts

class Executor(Task) :

    def __init__(self, on, end, name, timeout=-1) :
        Task.__init__(self, name, timeout)
        self.group  = on
        self.end    = end

    def terminate_task(self) :
        if self.end :
            self.group.terminate_task()
        Task.terminate_task(self)

    def print_data(self, data, prefix) :
        decoded = json.loads(data)
        self.printt_decoded_data(decoded["stderr"], decoded["stdout"],
                                 decoded["status"], prefix)

    def printt_decoded_data(self, stderr, stdout, status, prefix) :
        print("---------- {} ----------".format(prefix))
        value = stderr
        for line in value :
            line = line.split("error >")[1]
            print ("{} {} : {}".format(prefix, "stderr", line))
        value = stdout
        for line in value :
            line = line.split("output >")[1]
            print ("{} {} : {}".format(prefix, "stdout", line))
        print ("{} {} : {}".format(prefix, "status", status))
