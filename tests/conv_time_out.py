#!/usr/bin/env python3
import sys

def main(argv):
    import getopt
    try:
        options, argv = getopt.getopt(
                argv[1:],
                "p:f:m:M:s:",
                [
                    "prefixes=", "folder=","min=","max=","steps="
                    ]
                )


        xmin, xmax  = 0, 1
        step        = 50
        prefixes    = []
        folder_base = ""

        for option, value in options:
            if option in ["-m", "--min"]:
                xmin = int(value)
            elif option in ["-M", "--max"]:
                xmax = int(value)
            elif option in ["-s", "--steps"]:
                step = int(value)
            elif option in ["-p", "--prefixes"]:
                prefixes = value.split(",");
            elif option in ["-f", "--folder"]:
                folder_base = value+"/"

        out = open(folder_base+"experiment.csv", 'w')
        out.write("y,x,type\n")

        for local_max in range(xmin, xmax+1) :
            if local_max % step == 0 :
                for prefix in prefixes :
                    f     = open(folder_base+prefix+str(local_max)+".out", 'r')
                    lines = f.readlines()
                    for line in lines :
                        line = line.replace("\n","").replace("\r","")
                        out.write(line+","+str(local_max)+","+prefix+"\n")
                    f.close()
        out.close()

    except getopt.GetoptError as message:
        sys.stdout.write("toto")

if __name__ == "__main__":
    sys.exit(main(sys.argv))
