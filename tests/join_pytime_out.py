#!/usr/bin/env python3
import sys

def main(argv):
    import getopt
    try:
        options, argv = getopt.getopt(
                argv[1:],
                "p:f:m:M:s:",
                [
                    "prefixes=", "folder=","min=","max=","steps="
                    ]
                )


        xmin, xmax  = 0, 1
        step        = 50
        folder_base = ""

        for option, value in options:
            if option in ["-m", "--min"]:
                xmin = int(value)
            elif option in ["-M", "--max"]:
                xmax = int(value)
            elif option in ["-s", "--steps"]:
                step = int(value)
            elif option in ["-f", "--folder"]:
                folder_base = value+"/"

        out = open(folder_base+"internal.csv", 'w')
        out.write("x;y;type\n")

        for local_max in range(xmin, xmax+1) :
            if local_max % step == 0 :
                f     = open(folder_base+str(local_max)+".time", 'r')
                lines = f.readlines()
                first = True
                for line in lines :
                    if first :
                        first = False
                    else :
                        line = line.replace("\n","").replace("\r","")
                        out.write(str(local_max)+";"+line+"\n")
                f.close()
        out.close()

    except getopt.GetoptError as message:
        sys.stdout.write("toto")

if __name__ == "__main__":
    sys.exit(main(sys.argv))
