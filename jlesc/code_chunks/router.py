def start(self, networkId):
    def network_root_up(error_nodes):
        def network_root_update(data):
            def done(data) :
                self.close()
            # Notify execution termination on done
            self.broadcast_exec_on("0", "uptime",
                                   consts.TRUE,
                                   "0", "root",
                                   "0", "root", done)
        # Notify numbering success on network_root_update
        self.network_update_on(consts.TRUE, "0", "root",
                 "0","root", network_root_update)
    # Notify spawn success on network_root_up
    self.spawn_on("0",self.node_list, consts.FALSE, "0", "root",
                  "0", "root", network_root_up)
