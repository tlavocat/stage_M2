def register_tasks(self) :
    # Start a new group on a given computer list.
    # Group leader attached to the root.
    g1 = Group("g1", "g1", self.node_list, "0", "root")

    # Make a simple broadcast on the group.
    # broadcast termination implies group termination.
    b1 = Broadcaster("b1", "uptime", g1, True)

    # Broadcast starts when the group is read.
    b1.add_dependency({g1:consts.RUNING}, True)

    # register all the generated task.
    self.tasks.append(g1)
    self.tasks.append(b1)
