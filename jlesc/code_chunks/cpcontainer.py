# Compute client and server location
path        = os.environ["PWD"]
server_path = "{}/../c_wrapper/server -C {}".format(path, nb_clients)
client_path = "{}/../c_wrapper/client -R 1 -S {}".format(path, "server")

class Sample(TaskProcessor):

    def register_tasks(self) :
        nb_clients = 10

        # Create a CPContainer for the server, on a computer list
        sc = CPContainer("jail_server", 'server', 'A,B,C', '0', 'root')

        #Launch server
        server = CPExecutor(
                's1',
                server_path,
                sc,
                True)

        # start when the cpcontainer is running
        server.add_dependency({sc:consts.RUNING}, True)
        self.tasks.append(sc)
        self.tasks.append(server)

        # Launch nb_clients Clients
        for i in range(0, nb_clients) :
            # Create a CPContainer for the client
            cc = CPContainer(
                    "jail_client{}".format(i),
                    "client{}".format(i),
                    'D,E,F',
                    '0',
                    'root')

            # Launch the client
            client = CPExecutor(
                    "mpi_executor{}".format(i),
                    client_path,
                    cc,
                    True)

            # Only start the client when the server is running and when the
            # cpcontainer is running
            client.add_dependency(
                    {cc:consts.RUNING,server:consts.RUNING}, True)
            self.tasks.append(cc)
            self.tasks.append(client)
