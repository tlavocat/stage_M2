\documentclass[10pt]{beamer}
\usetheme{Boadilla}
\usepackage[english]{babel}
\usepackage{color}
\usepackage{caption}    	% Légendes avancées
\usepackage{float}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{framed}
\captionsetup[figure]{font=small, skip=10pt}
\usepackage{graphics}

\setbeamerfont{page number in head/foot}{size=\tiny}
\setbeamertemplate{footline}[frame number]

\usepackage{listings}
\usepackage{color}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\newcommand{\backupbegin}{
   \newcounter{finalframe}
   \setcounter{finalframe}{\value{framenumber}}
}
\newcommand{\backupend}{
   \setcounter{framenumber}{\value{finalframe}}
}

\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Octave,                 % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,	                   % sets default tabsize to 2 spaces
}

\author{Thomas Lavocat, supervised by Olivier Richard and Guillaume Huard}
\title{A Recursively Distributed Remote Execution Engine}
%\setbeamercovered{transparent} 
%\setbeamertemplate{navigation symbols}{} 
%\logo{} 
\institute{UGA Inria Lig, Datamove, Polaris.\\
In collaboration with Swann Perarnau Argonne National Laboratory (USA)}
\date{July 2017}
%\subject{}
\begin{document}

\begin{frame}
    \titlepage
\end{frame}

\section{Introduction}

\begin{frame}{Context illustration}{LibEnsemble Use Case}
        \center
        \begin{minipage}[c]{0.5\textwidth}
            \includegraphics[width=1\linewidth]{pics/optimisation_1.png}
        \end{minipage}%
        \begin{minipage}[c]{0.5\textwidth}
            \begin{itemize}
                \item Optimisation problem
                \item Find the minimum
            \end{itemize}
        \end{minipage}%
\end{frame}

\begin{frame}{Context illustration}{LibEnsemble Use Case}
        \center
        \begin{minipage}[c]{0.5\textwidth}
            \includegraphics[width=1\linewidth]{pics/optimisation_2.png}
        \end{minipage}%
        \begin{minipage}[c]{0.5\textwidth}
            \begin{itemize}
                \item Generate some path to explore
                \item Launch some processing, and wait for results
                \item Terminate executions upon timeout
            \end{itemize}
        \end{minipage}%
\end{frame}

\begin{frame}{Context illustration}{LibEnsemble Use Case}
        \center
        \begin{minipage}[c]{0.5\textwidth}
            \includegraphics[width=1\linewidth]{pics/optimisation_3.png}
        \end{minipage}%
        \begin{minipage}[c]{0.5\textwidth}
            \begin{itemize}
                \item Refine results
                \item Launch other simulation reusing the same computation
                    nodes
            \end{itemize}
        \end{minipage}%
\end{frame}

\begin{frame}{Context illustration}{LibEnsemble Use Case}
        \center
        \begin{minipage}[c]{0.5\textwidth}
            \includegraphics[width=1\linewidth]{pics/optimisation_4.png}
        \end{minipage}%
        \begin{minipage}[c]{0.5\textwidth}
            \begin{itemize}
                \item Upon success
                \item Terminate unnecessary computations.
            \end{itemize}
        \end{minipage}%
\end{frame}

\begin{frame}{This Problem Needs}
    \begin{itemize}
        \item Partition a job allocation
            \begin{itemize}
                \item Reserve some nodes to compute
                \item Reserve one node to control the execution
            \end{itemize}
        \item Fine grained life cycle control
            \begin{itemize}
                \item Spawn jobs
                \item Kill non terminating jobs
                \item Gather computation results
                \item Recycle nodes after use
            \end{itemize}
        \item On-the-fly control of the simulations
            \begin{itemize}
                \item Message exchange between applications and controller
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Our solution}
    \begin{itemize}
        \item A root script controlling the execution
            \begin{itemize}
                \item Python recipes to control executions
            \end{itemize}
        \item Connect and exploit nodes using a remote parallel fork (TakTuk)
            \begin{itemize}
                \item Partition allocation
                \item Fine grained control over executions
            \end{itemize}
        \item Expose a distributed IPC to remotely launched programs
            \begin{itemize}
                \item Allowing remote applications to exchange control messages
                    between each others
            \end{itemize}
    \end{itemize}
\end{frame}

\begin{frame}{Our solution}{Groups}
    \includegraphics[width=.5\linewidth]{pics/design_tree.png}
\end{frame}

\begin{frame}{Integration on Melissa and Decaf}{Simple Discovery Problem}
    \begin{figure}[!t]
        \begin{minipage}[c]{0.5\textwidth}
            \includegraphics[width=0.9\linewidth]{pics/melissa.png}
            \label{fig:melissa}
        \end{minipage}%
        \begin{minipage}[c]{0.5\textwidth}
            \begin{block}{Current improvements}
                \begin{itemize}
                    \item Improve sub-parts discovery
                    \item Connection information retrieval
                    \item Remove the NFS dependency
                \end{itemize}
            \end{block}
            \begin{exampleblock}{Patch size}
                \begin{itemize}
                    \item Around 30 lignes of C code for each solution
                \end{itemize}
            \end{exampleblock}
        \end{minipage}
    \end{figure}
\end{frame}

\begin{frame}{Ongoing Integration on Decaf}{Advanced}
    \includegraphics[width=.8\linewidth]{pics/integration_workflow.png}
\end{frame}

\begin{frame}{Collaborations}
    \begin{block}{What we have~:}
        \begin{itemize}
            \item Scriptable deployment solution
            \item Execution life-cycle control
            \item Inter-application communications
            \item Already working integration examples on simple use cases
        \end{itemize}
    \end{block}
    \begin{block}{What we need~:}
        \begin{itemize}
            \item Application users, to validate our solution
            \item To speak with scheduler's developers to integrate our
                solution
            \item Explore resilience strategies
        \end{itemize}
    \end{block}
\end{frame}

\section{Conclusion}
\end{document}
