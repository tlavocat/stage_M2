New repository for the project now named Yggdrasil :

* https://gitlab.com/lavocat/yggdrasil             main code repo
* https://gitlab.com/lavocat/yggdrasil-c           c_wrapper library
* https://gitlab.com/lavocat/yggdrasil-integration various tests

# Installation

## Requirements

First clone this repo somewhere on your computer.
Lets say `~/Documents/inria/m2` for instance.

To make things easier for you, you can update your bashrc (or whatever
equivalent) with those lines :

```
export PATH=$PATH:$GOPATH/bin:~/Documents/inria/m2/python:~/Documents/inria/m2/python/isengard:~/Documents/inria/m2/python/erebor:~/Documents/inria/m2/python/task_lib
export PYTHONPATH=$PYTHONPATH:~/Documents/inria/m2/python:~/Documents/inria/m2/python/isengard:~/Documents/inria/m2/python/erebor:~/Documents/inria/m2/python/task_lib
```

If you dont, it's okay, there is a special launcher able to execute the program
without the path setup.

### Needed softwares/libraries :

* taktuk `sudo aptitude install taktuk`
* python3 `sudo aptitude install python3`
* pyzmq `sudo aptitude install python3-zmq`
* pexpect `sudo aptitude install python3-pexpect`

To build the c_wrapper

* cmake
* cmake-curses-gui
* pkg-config
* libczmq-dev

### Local password less ssh key

It is better if this key is called id_local, it will be fetched by the kameleon
recipe to build the G5K test VM.

You can find my id_local on the ssh folder of this project.

### SSH config file containing

```
Host A
    HostName        A
    IdentityFile    /home/$USERNAME$/.ssh/id_local
Host B
    HostName        B
    IdentityFile    /home/$USERNAME$/.ssh/id_local
Host C
    HostName        C
    IdentityFile    /home/$USERNAME$/.ssh/id_local
Host D
    HostName        D
    IdentityFile    /home/$USERNAME$/.ssh/id_local
Host E
    HostName        E
    IdentityFile    /home/$USERNAME$/.ssh/id_local
Host F
    HostName        F
    IdentityFile    /home/$USERNAME$/.ssh/id_local
```

Replacing $USERNAME$ by your user name.

### /etc/hosts containing

```
127.0.1.1	A
127.0.1.1	B
127.0.1.1	C
127.0.1.1	D
127.0.1.1	E
127.0.1.1	F
127.0.1.1	G
127.0.1.1	H
127.0.1.1	I
127.0.1.1	J
127.0.1.1	K
127.0.1.1	L
127.0.1.1	M
```

### Build C examples

```
cd c_wrapper
ccmake ./
```

Type `c` to configure, make sure `BUILD_WITH_ZMQ` is ON and `ZMQ_DIR` points
where czmq is installed. Then `g` to generate.

```
make
```

### SSHD config

Ensure no max session is set.

## Testing installation

If everything is done properly, you should be able to run the `./regression.sh`
script in the python folder. If not, and if you are sure of your configuration,
try to run each example one after the other to see what is wrong and consider
adding an issue in the bug tracker.

# Coding

## Pre-push hook

Please consider adding this pre-push hook in `.git/hooks/pre-push` :

```
cd python
./regression.sh
exit $?
```

# Testing

## local testsing

Basically store your tests in the `python/tests` folder.
Launch them with the command `./launcher tests/your_test`
Once the test is good, consider adding it inside regression.sh for automatic
testing.

## G5K testing

A kameleon recipe is given to build a VM to be deployed on Grid'5000.
The buid_wm.sh script is not up to date.
In the journal there are steps to build a proper VM from the recipe.

# Publications

This research project has been made for my graduate. You can find in some
folders things that aren't code :

* slides, presentation for my defense
* report, core of my work for my defense
* papers, state of the art

# Journal (French section)

Dans le dossier journal se trouve le journal que je maintiens pour ce projet. Il
est en RMarkdown. Je vous laisse suivre la documentation sur ce site pour être
capable de compiler le document : http://rmarkdown.rstudio.com/

Une version au format HTML est disponible dans le dossier journal. J'essayerai
de la commiter assez régulièrement.

Disclaimer : Je marque un peu tout ce qui me passe pas la tête dans ce document,
ne vous étonnez, ni de sa longueur, ni de ses redondances, ni du langage
informel qui y est employé.
