---
name: jessie-x64-nfs-6
version: 2017030114
description: Debian Jessie (nfs)
author: support-staff@lists.grid5000.fr
visibility: private
destructive: false
os: linux
image:
  file: /home/tlavocat/Documents/inria/m2/kameleon/build/jessie_custom/jessie_custom.tar.gz
  kind: tar
  compression: gzip
postinstalls:
- archive: server:///grid5000/postinstalls/debian-x64-nfs-2.6-post.tgz
  compression: gzip
  script: traitement.ash /rambin
boot:
  kernel_params: ""
  kernel: /vmlinuz
  initrd: /initrd.img
filesystem: ext4
partition_type: 131
multipart: false
