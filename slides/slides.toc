\select@language {english}
\beamer@sectionintoc {1}{Introduction}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{State-of-the-Art}{7}{0}{1}
\beamer@sectionintoc {2}{Evaluation}{12}{0}{2}
\beamer@subsectionintoc {2}{1}{Melissa, An InSitu Application}{18}{0}{2}
\beamer@sectionintoc {3}{Conclusion}{20}{0}{3}
\beamer@subsectionintoc {3}{1}{Conclusion}{21}{0}{3}
\beamer@subsectionintoc {3}{1}{User API}{27}{1}{3}
\beamer@subsectionintoc {3}{2}{API Usability}{28}{1}{3}
