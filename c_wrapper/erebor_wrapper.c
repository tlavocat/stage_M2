#include "erebor_wrapper.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <zmq.h>
#include <arpa/inet.h>
#include <stdbool.h>

static void *put_bytes(void *pos, const void *mem, size_t length);
static void *put_uint32(void *pos, uint32_t value);
int pack(int nb, char** params, char*pack);
int wait_size(char* datas, int* offset);
int send_to(erebor* erebor, char* dest, char*group, char* message, char* type, bool block);
int get_msg(erebor* erebor, char*dest, char* group, char*message, zmq_msg_t*msg);

void erebor_print_error(){
    switch(errno){
        case EAGAIN :
            printf("Non-blocking mode was requested and the message cannot  \
                    be sent at the moment.\n");
            break;
        case ENOTSUP :
            printf("The zmq_send() operation is not supported by this socket type.\n");
            break;
        case EFSM :
            printf("The zmq_send() operation cannot be performed on this socket \
                    at the moment due to the socket not being in the appropriate \
                    state. This error may occur with socket types that switch \
                    between several states, such as ZMQ_REP. \
                    See the messaging patterns section \
                    of zmq_socket(3) for more information.\n");
            break;
        case EINTR :
            printf("The operation was interrupted by delivery of a signal before the message was sent.\n");
            break;
        case EHOSTUNREACH :
            printf("The message cannot be routed.\n");
            break;
        case EINVAL :
            printf("The endpoint supplied is invalid.");
            break;
        case EPROTONOSUPPORT :
            printf("The requested transport protocol is not supported.");
            break;
        case ENOCOMPATPROTO :
            printf("The requested transport protocol is not compatible with the socket type.");
            break;
        case ETERM :
            printf("The 0MQ context associated with the specified socket was terminated.");
            break;
        case ENOTSOCK :
            printf("The provided socket was invalid.");
            break;
        case EMTHREAD :
            printf("No I/O thread is available to accomplish the task.");
            break;
        case EADDRINUSE :
            printf("The requested address is already in use.");
            break;
        case EADDRNOTAVAIL :
            printf("The requested address was not local.");
            break;
        case ENODEV :
            printf("The requested address specifies a nonexistent interface.");
            break;
    }
}

static void *put_bytes(void *pos, const void *mem, size_t length) {
    memcpy(pos, mem, length);
    return ((char *) pos) + length;
}

static void *put_uint32(void *pos, uint32_t value) {
    uint32_t to_be_sent;
    char *src, *dst;
    int size = sizeof(uint32_t);
    to_be_sent = htonl(value);
    src = (char *) &to_be_sent;
    dst = (char *) pos;
    while (size--)
        *(dst++) = *(src++);
    return dst;
}


int erebor_init_connection(char* str_rank, char* network, char* in_ipc, char* out_ipc, erebor* erebor)
{
    return erebor_init_linger_connection(str_rank, network, in_ipc, out_ipc, erebor, 0);
}

int erebor_init_linger_connection(char* str_rank, char* network, char* in_ipc, char* out_ipc, erebor* erebor, int linger)
{
    erebor->buff    = malloc(5);
    erebor->rank    = malloc(strlen(str_rank)+1);
    erebor->network = malloc(strlen(network)+1);
    erebor->context = zmq_ctx_new();
    erebor->puller  = zmq_socket (erebor->context, ZMQ_PULL);
    erebor->pusher  = zmq_socket (erebor->context, ZMQ_PUSH);
    zmq_setsockopt(erebor->puller, ZMQ_LINGER, &linger, sizeof(linger));
    zmq_setsockopt(erebor->pusher, ZMQ_LINGER, &linger, sizeof(linger));

    sprintf(erebor->rank,    "%s", str_rank);
    sprintf(erebor->network, "%s",  network);

    if(erebor->context == NULL)
        return -1;
    if(erebor->puller == NULL)
        return -1;
    if(erebor->pusher == NULL)
        return -1;

    char erebor_buffer[100];
    //connect puller
    sprintf (erebor_buffer, "ipc:///tmp/%s", out_ipc);
    if (zmq_bind (erebor->puller, erebor_buffer) == -1){
        return -1;
    }
    //printf("puller bound to %s\n", erebor_buffer);
    //connect pusher
    sprintf (erebor_buffer, "ipc:///tmp/%s", in_ipc);
    if(zmq_connect (erebor->pusher, erebor_buffer) == -1){
        return -1;
    }
    //printf("pusher connected to %s\n", erebor_buffer);

    char* params[3];
    params[0] = "register";
    params[1] = erebor->rank;
    params[2] = out_ipc;

    //printf("type %s \n", params[0]);
    //printf("rank %s \n", params[1]);
    //printf("socket %s \n", params[2]);

    int data_len = strlen(params[0])+strlen(params[1])+strlen(params[2])+12;

    //printf("data len %d\n", data_len);
    char* pck = malloc(data_len);
    //printf("going to pack\n");
    int len = pack(3, params, pck);
    int ret = zmq_send (erebor->pusher, pck, len, 0);
    free(pck);
    return ret;
}

int pack(int nb_params, char** params, char*pck){
    int   len  = 0;
    char* tram = pck;
    for(int i=0; i<nb_params; i++){
        tram = put_uint32(tram, strlen(params[i]));
        tram = put_bytes(tram, params[i], strlen(params[i]));
        len += 4 + strlen(params[i]);
    }
    return len;
}

int wait_size(char* datas, int* offset){
    char* place = &datas[*offset];
    int*  sizep = (int*) place;
    int   len   = ntohl(sizep[0]);
    *offset += 4;
    return len;
}

int erebor_recv(erebor* erebor, char*dest, char* group, char*message){
    zmq_msg_t msg;
    int rc = zmq_msg_init (&msg);
    if (rc != 0){
        return rc;
    }
    rc = zmq_recvmsg (erebor->puller, &msg, 0);
    if (rc == -1){
        return rc;
    }
    else{
        return get_msg(erebor, dest, group, message, &msg);
    }
}

int erebor_non_block_recv(erebor* erebor, char*dest, char* group, char*message){
    zmq_msg_t msg;
    int rc = zmq_msg_init (&msg);
    if (rc != 0){
        return rc;
    }
    rc = zmq_recvmsg (erebor->puller, &msg, ZMQ_DONTWAIT);
    if (rc == -1){
        if(errno == EAGAIN){
            return -2;
        }
        else{
            return -1;
        }
    }else{
        return get_msg(erebor, dest, group, message, &msg);
    }
}

int get_msg(erebor* erebor, char*dest, char* group, char*message, zmq_msg_t*msg){
    char* datas = (char*) zmq_msg_data (msg);
    //printf("wait to receive dest\n");
    int offset = 0;
    //dest
    int len = wait_size(datas, &offset);
    memcpy(dest, &datas[offset], len);
    offset += len;
    dest[len] = 0x00;
    //group
    len = wait_size(datas, &offset);
    memcpy(group, &datas[offset], len);
    offset += len;
    group[len] = 0x00;
    //message
    len = wait_size(datas, &offset);
    memcpy(message, &datas[offset], len);
    offset += len;
    message[len] = 0x00;
    zmq_msg_close (msg);
    return 0;
}

int erebor_non_block_send_to(erebor* erebor, char* dest, char*group, char* message){
    return send_to(erebor, dest, group, message, "mpi send", false);
}

int erebor_send_to(erebor* erebor, char* dest, char*group, char* message){
    return send_to(erebor, dest, group, message, "mpi send", true);
}

int erebor_non_block_ctrl_msg_to(erebor* erebor, char*group, char* message){
    return send_to(erebor, "0", group, message, "message", false);
}

int erebor_ctrl_msg_to(erebor* erebor, char*group, char* message){
    return send_to(erebor, "0", group, message, "message", true);
}


int send_to(erebor* erebor, char* dest, char*group, char* message, char* type,
                                                                    bool block){
    char* params[6];
    params[0] = type;
    params[1] = dest;
    params[2] = group;
    params[3] = erebor->rank;
    params[4] = erebor->network;
    params[5] = message;

    //printf("type %s \n", params[0]);
    //printf("dest %s \n", params[1]);
    //printf("group %s \n", params[2]);
    //printf("rank %s \n", params[3]);
    //printf("group %s \n", params[4]);
    //printf("message %s \n", params[5]);

    int data_len = strlen(params[0])+strlen(params[1])+strlen(params[2])+
            strlen(params[3])+ strlen(params[4])+ strlen(params[5])+(6*4);
    char* pck = malloc(data_len);
    int len = pack(6, params, pck);
    int ret =  0;
    if(block){
        ret = zmq_send (erebor->pusher, pck, len, 0);
        free(pck);
        return ret;
    }else{
        ret = zmq_send (erebor->pusher, pck, len, ZMQ_DONTWAIT);
        if(ret == -1){
            if(errno == EAGAIN){
                return -2;
            }else{
                return -1;
            }
        }
        free(pck);
        return ret;
    }
}

void erebor_close(erebor* erebor){
    free(erebor->rank);
    free(erebor->network);
    zmq_close(erebor->pusher);
    zmq_close(erebor->puller);
    zmq_ctx_term(erebor->context);
}
