#include <stdio.h>
#include "erebor_wrapper.h"
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h>

int main(int argc, char** argv){
    int opt;
    char* in_ipc;
    char* out_ipc = malloc(100);
    char* network;
    int rank =0;
    char* str_rank = "0";
    int c =0;
    do
    {
        opt = getopt (argc, argv, "P:N:C:");
        switch (opt) {
            case 'C':
                c = atoi(optarg);
                break;
            case 'P':
                in_ipc = optarg;
                int iout_ipc = atoi(optarg) + 100 + rank;
                sprintf(out_ipc, "%d", iout_ipc);
                break;
            case 'N':
                network = optarg;
                break;
        }
    } while (opt != -1);
    printf("server %s %s %s %d\n", in_ipc, out_ipc, network, c);
    erebor* e = malloc(sizeof(erebor));
    int ret = erebor_init_connection(str_rank, network, in_ipc, out_ipc, e);
    if (ret == -1){
        erebor_print_error();
    }else{
        printf("ret %d\n", ret);
        char *dest    = malloc(100);
        char *group   = malloc(100);
        char *message = malloc(100);
        for(int i=0; i<c; i++){
            printf("wait message %d\n", i);
            erebor_recv(e, dest, group, message);
            printf("received message %s from %s@%s\n", message, dest, group);
            erebor_send_to(e, dest, group, "well received");
        }
    }
    erebor_close(e);
}
